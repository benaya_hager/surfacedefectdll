// SetupDialogMFC.cpp : implementation file
//

#include "stdafx.h"
#include "SetupDialogMFC.h"
#include "SliderGage.h"

#define MAX_GROUP_COUNT 20
#define MAX_PARAM_COUNT 100
#define LEFT_MARGIN 10
#define TOP_MARGIN 10
#define BOTTOM_MARGIN 10
#define CAPTION_WIDTH 250
#define CAPTION_HEIGHT 40
#define SLIDER_WIDTH 250
#define SLIDER_HEIGHT 40
#define STRING_WIDTH 250
#define STRING_HEIGHT 20
#define CONTROL_VERT_STEP 50
#define COMBO_BOX_WIDTH 150
#define COMBO_BOX_HEIGHT 150


HHOOK hHook=NULL ;

// Hook procedure for WH_GETMESSAGE hook type.
LRESULT CALLBACK GetMessageProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	// Switch the module state for the correct handle to be used.
	AFX_MANAGE_STATE(AfxGetStaticModuleState( ));



	// If this is a keystrokes message, translate it in controls'
	// PreTranslateMessage().
	LPMSG lpMsg = (LPMSG) lParam;
	if( (nCode>=0) &&
		(PM_REMOVE==wParam) &&
		( (lpMsg->message>=WM_KEYFIRST && lpMsg->message<=WM_KEYLAST) ||
		  lpMsg->message==WM_MOUSEMOVE ) &&
		AfxGetApp()->PreTranslateMessage((LPMSG)lParam) )
	{
		// The value returned from this hookproc is ignored, and it cannot
		// be used to tell Windows the message has been handled. To avoid
		// further processing, convert the message to WM_NULL before
		// returning.
		/*
		lpMsg->message = WM_NULL;
		lpMsg->lParam = 0L;
		lpMsg->wParam = 0;
		*/
	}

	// Passes the hook information to the next hook procedure in
	// the current hook chain.
	return ::CallNextHookEx(hHook, nCode, wParam, lParam);
}


// CSetupDialogMFC

IMPLEMENT_DYNAMIC(CSetupDialogMFC, CPropertySheet)

CSetupDialogMFC::CSetupDialogMFC(LPCTSTR pszCaption, CParams &params, CWnd* pParentWnd, UINT iSelectPage)
	:CPropertySheet(pszCaption, pParentWnd, iSelectPage)
{
	m_pParentWindow = pParentWnd ;
	m_params = params ;
	m_captionWidth = CAPTION_WIDTH ;
	m_captionHeight = CAPTION_HEIGHT ;
	AddSetupPages() ;
}

CSetupDialogMFC::~CSetupDialogMFC()
{
	for ( unsigned i=0 ; i<m_propPages.size() ; i++ ){
		if (NULL!=m_propPages[i]) delete m_propPages[i] ;
	}
}



void CSetupDialogMFC::AddSetupPages()
{
	m_psh.dwFlags |= PSP_USEHICON ;
	m_psh.dwFlags |= PSH_NOAPPLYNOW; // Eliminate the Apply button
	m_psh.dwFlags &= ~PSH_HASHELP; // Eliminate the Help button
	
	m_propPages.clear() ;
	for ( size_t gi=0 ; gi<m_params.groups.size() ; gi++ ){
		if (m_params.groups[gi].name != string("Setup Dialog")){
			CPropertyPageSmall *pPage = new CPropertyPageSmall ;
			pPage->m_psp.dwFlags &= ~PSP_HASHELP ;
			pPage->m_index = int(gi) ;
			pPage->m_params = m_params.groups[gi] ;
			m_propPages.push_back(pPage) ;
			AddPage( m_propPages[gi] ) ;
		}
	}
}


BOOL CSetupDialogMFC::OnInitDialog()
{
	int dx=0, dy=0 ;
	for ( unsigned i=0 ; i<m_propPages.size() ; i++ ){
		// Calculate the size of the page
		CPropertyPageSmall *pPage = m_propPages[i] ;
		int height = TOP_MARGIN +
					 int(pPage->m_params.members.size())*CONTROL_VERT_STEP +
					 SLIDER_HEIGHT +
					 BOTTOM_MARGIN ;
		if (dy<height) dy=height ;
	}
	int captionWidth = m_params.GetIntValue("Setup Dialog","Caption Width") ;
	int captionHeight = m_params.GetIntValue("Setup Dialog","Caption Height") ;
	if (captionWidth>0) m_captionWidth = captionWidth ;
	if (captionHeight>0) m_captionHeight = captionHeight ;
	/*
	dx = 2*LEFT_MARGIN + CAPTION_WIDTH + max(STRING_WIDTH,max(COMBO_BOX_WIDTH,SLIDER_WIDTH)) ; 
	*/
	dx = 2*LEFT_MARGIN + m_captionWidth + max(STRING_WIDTH,max(COMBO_BOX_WIDTH,SLIDER_WIDTH)) ; 
	for ( unsigned i=0 ; i<m_propPages.size() ; i++ ){
		m_propPages[i]->m_captionWidth = m_captionWidth ;
		m_propPages[i]->m_captionHeight = m_captionHeight ;
		m_propPages[i]->m_dx=dx ;
		m_propPages[i]->m_dy=dy ;
	}

	BOOL bResult = CPropertySheet::OnInitDialog();

	// Install the WH_GETMESSAGE hook function.
	hHook = ::SetWindowsHookEx(
		WH_GETMESSAGE,
		GetMessageProc,
		AfxGetInstanceHandle(),
		GetCurrentThreadId());
	ASSERT (hHook);

	return bResult;
}

void CSetupDialogMFC::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CPropertySheet::OnShowWindow(bShow, nStatus);

	for ( unsigned i=0 ; i<m_propPages.size() ; i++ ){
		TC_ITEM tcItem;
		tcItem.mask = TCIF_TEXT;
		CString str(m_params.groups[i].name.c_str()) ;
		tcItem.pszText = (LPTSTR)((LPCTSTR)(str));
		GetTabControl()->SetItem( i, &tcItem );
	}

	// Adjust the property sheet
	int dx = m_propPages[0]->m_dx ;
	int dy = m_propPages[0]->m_dy ;
	CRect sheetRect ;
	GetWindowRect(&sheetRect) ;
	SetWindowPos( NULL, 0, 0,
					sheetRect.Width()+dx,
					sheetRect.Height()+dy,
					SWP_NOZORDER|SWP_NOMOVE ) ;
	int buttonIDs[] = {IDOK,IDCANCEL} ;
	for ( int i=0 ; i<sizeof(buttonIDs)/sizeof(buttonIDs[0]) ; i++ ){
		CWnd *pWnd = GetDlgItem(buttonIDs[i]) ;
		CRect rect ;
		pWnd->GetWindowRect(&rect) ;
		ScreenToClient( &rect ) ;
		pWnd->SetWindowPos( NULL, rect.left+dx, rect.top+dy, 0, 0, SWP_NOZORDER|SWP_NOSIZE ) ;
	}
}


BEGIN_MESSAGE_MAP(CSetupDialogMFC, CPropertySheet)
	ON_WM_SHOWWINDOW()
	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CPropertyPageSmall dialog

IMPLEMENT_DYNAMIC(CPropertyPageSmall, CPropertyPage)

CPropertyPageSmall::CPropertyPageSmall()
	: CPropertyPage(CPropertyPageSmall::IDD)
	, m_pToolTip(NULL)
	, m_index(-1)
	, m_captionWidth(CAPTION_WIDTH)
	, m_captionHeight(CAPTION_HEIGHT)
{

}

CPropertyPageSmall::~CPropertyPageSmall()
{
	for ( size_t i=0 ; i<m_controls.size() ; i++ ){
		if (m_controls[i].first!=NULL) delete m_controls[i].first ;
		if (m_controls[i].second!=NULL) delete m_controls[i].second ;
	}
	m_controls.clear() ;
	delete m_pToolTip ;
}

void CPropertyPageSmall::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CPropertyPageSmall, CPropertyPage)
	ON_BN_CLICKED(IDC_ACCEPT, &CPropertyPageSmall::OnBnClickedAccept)
	ON_WM_SHOWWINDOW()
	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CPropertyPageSmall message handlers

BOOL CPropertyPageSmall::OnInitDialog()
{
	CPropertyPage::OnInitDialog();

	// Install the WH_GETMESSAGE hook function.
	hHook = ::SetWindowsHookEx( WH_GETMESSAGE,
		GetMessageProc,
		AfxGetInstanceHandle(),
		GetCurrentThreadId() );

	CFont *pCaptionFont = GetFont() ;
	for ( int i=0 ; i<int(m_params.members.size()) ; i++ ){
		CParam *pParam = m_params.members[i] ;
		int IDC = int(IDC_BASE+IDC_PAGE_STEP*m_index+i) ;
		CParam::Type paramType = pParam->GetType() ;
		CString paramName = CString(pParam->name.c_str()) ;
		switch(paramType){
		case CParam::INT:
			{
				CSliderGage *pSliderGage = new(CSliderGage) ;
				CStatic *pCaption = new(CStatic) ;
				m_controls.push_back(pair<CStatic*,CWnd*>(pCaption,pSliderGage)) ;
				pCaption->Create(paramName, WS_CHILD|WS_VISIBLE,
								 /*
								 CRect(LEFT_MARGIN,TOP_MARGIN+i*CONTROL_VERT_STEP,LEFT_MARGIN+CAPTION_WIDTH,TOP_MARGIN+i*CONTROL_VERT_STEP+CAPTION_HEIGHT),
								 */
								 CRect(LEFT_MARGIN,TOP_MARGIN+i*CONTROL_VERT_STEP,LEFT_MARGIN+m_captionWidth,TOP_MARGIN+i*CONTROL_VERT_STEP+m_captionHeight),
								 this, IDC_STATIC ) ;
				pCaption->SetFont( pCaptionFont ) ;
				pSliderGage->m_isInteger = true ;
				CIntParam *pIntParam = (CIntParam*)pParam ;
				pSliderGage->m_intMin = pIntParam->minVal ;
				pSliderGage->m_intMax = pIntParam->maxVal ;
				pSliderGage->m_intVal = pIntParam->val ;
				pSliderGage->Create(NULL,CString("SliderGage"),WS_CHILD|WS_VISIBLE,
									/*
									CRect(LEFT_MARGIN+CAPTION_WIDTH,i*CONTROL_VERT_STEP,LEFT_MARGIN+CAPTION_WIDTH+SLIDER_WIDTH,i*CONTROL_VERT_STEP+SLIDER_HEIGHT),
									*/
									CRect(LEFT_MARGIN+m_captionWidth,i*CONTROL_VERT_STEP,LEFT_MARGIN+m_captionWidth+SLIDER_WIDTH,i*CONTROL_VERT_STEP+SLIDER_HEIGHT),
									this, IDC_STATIC ) ;
				pSliderGage->DrawControls() ;
			}
			break ;
		case CParam::DBL:
			{
				CSliderGage *pSliderGage = new(CSliderGage) ;
				CStatic *pCaption = new(CStatic) ;
				m_controls.push_back(pair<CStatic*,CWnd*>(pCaption,pSliderGage)) ;
				pCaption->Create(paramName, WS_CHILD|WS_VISIBLE,
								 /*
								 CRect(LEFT_MARGIN,TOP_MARGIN+i*CONTROL_VERT_STEP,LEFT_MARGIN+CAPTION_WIDTH,TOP_MARGIN+i*CONTROL_VERT_STEP+CAPTION_HEIGHT),
								 */
								 CRect(LEFT_MARGIN,TOP_MARGIN+i*CONTROL_VERT_STEP,LEFT_MARGIN+m_captionWidth,TOP_MARGIN+i*CONTROL_VERT_STEP+m_captionHeight),
								 this, IDC_STATIC ) ;
				pCaption->SetFont( pCaptionFont ) ;
				pSliderGage->m_isInteger = false ;
				CDoubleParam *pDblParam = (CDoubleParam*)pParam ;
				pSliderGage->m_dblMin = pDblParam->minVal ;
				pSliderGage->m_dblMax = pDblParam->maxVal ;
				pSliderGage->m_dblVal = pDblParam->val ;
				pSliderGage->Create(NULL,CString("SliderGage"),WS_CHILD|WS_VISIBLE,
									/*
									CRect(LEFT_MARGIN+CAPTION_WIDTH,i*CONTROL_VERT_STEP,LEFT_MARGIN+CAPTION_WIDTH+SLIDER_WIDTH,i*CONTROL_VERT_STEP+SLIDER_HEIGHT),
									*/
									CRect(LEFT_MARGIN+m_captionWidth,i*CONTROL_VERT_STEP,LEFT_MARGIN+m_captionWidth+SLIDER_WIDTH,i*CONTROL_VERT_STEP+SLIDER_HEIGHT),
									this, IDC_STATIC ) ;
				pSliderGage->DrawControls() ;
			}
			break ;
		case CParam::STR:
			{
				CEdit *pString = new(CEdit) ;
				CStatic *pCaption = new(CStatic) ;
				m_controls.push_back(pair<CStatic*,CWnd*>(pCaption,pString)) ;
				pCaption->Create(paramName, WS_CHILD|WS_VISIBLE,
								 /*
								 CRect(LEFT_MARGIN,TOP_MARGIN+i*CONTROL_VERT_STEP,LEFT_MARGIN+CAPTION_WIDTH,TOP_MARGIN+i*CONTROL_VERT_STEP+CAPTION_HEIGHT),
								 */
								 CRect(LEFT_MARGIN,TOP_MARGIN+i*CONTROL_VERT_STEP,LEFT_MARGIN+m_captionWidth,TOP_MARGIN+i*CONTROL_VERT_STEP+m_captionHeight),
								 this, IDC_STATIC ) ;
				pCaption->SetFont( pCaptionFont ) ;
				pString->CreateEx( WS_EX_STATICEDGE, _T("EDIT"), NULL, WS_CHILD|WS_VISIBLE|ES_LEFT|WS_BORDER,
								   /*
								   CRect(LEFT_MARGIN+CAPTION_WIDTH,TOP_MARGIN+i*CONTROL_VERT_STEP,LEFT_MARGIN+CAPTION_WIDTH+STRING_WIDTH,TOP_MARGIN+i*CONTROL_VERT_STEP+STRING_HEIGHT),
								   */
								   CRect(LEFT_MARGIN+m_captionWidth,TOP_MARGIN+i*CONTROL_VERT_STEP,LEFT_MARGIN+m_captionWidth+STRING_WIDTH,TOP_MARGIN+i*CONTROL_VERT_STEP+STRING_HEIGHT),
								   this, IDC ) ;
				pString->SetFont( pCaptionFont ) ;
				CStringParam *pStrParam = (CStringParam*)pParam ;
				pString->SetWindowText( CString(pStrParam->val) ) ;

			}
			break ;
		case CParam::GRP:
			{
				CComboBox *pComboBox = new(CComboBox) ;
				CStatic *pCaption = new(CStatic) ;
				m_controls.push_back(pair<CStatic*,CWnd*>(pCaption,pComboBox)) ;
				pCaption->Create(paramName, WS_CHILD|WS_VISIBLE,
								 /*
								 CRect(LEFT_MARGIN,TOP_MARGIN+i*CONTROL_VERT_STEP,LEFT_MARGIN+CAPTION_WIDTH,TOP_MARGIN+i*CONTROL_VERT_STEP+CAPTION_HEIGHT),
								 */
								 CRect(LEFT_MARGIN,TOP_MARGIN+i*CONTROL_VERT_STEP,LEFT_MARGIN+m_captionWidth,TOP_MARGIN+i*CONTROL_VERT_STEP+m_captionHeight),
								 this, IDC_STATIC ) ;
				pCaption->SetFont( pCaptionFont ) ;
				pComboBox->Create( CBS_DROPDOWN|WS_VSCROLL|WS_VISIBLE,
								   /*
								   CRect(LEFT_MARGIN+CAPTION_WIDTH,TOP_MARGIN+i*CONTROL_VERT_STEP,LEFT_MARGIN+CAPTION_WIDTH+COMBO_BOX_WIDTH,TOP_MARGIN+i*CONTROL_VERT_STEP+COMBO_BOX_HEIGHT),
								   */
								   CRect(LEFT_MARGIN+m_captionWidth,TOP_MARGIN+i*CONTROL_VERT_STEP,LEFT_MARGIN+m_captionWidth+COMBO_BOX_WIDTH,TOP_MARGIN+i*CONTROL_VERT_STEP+COMBO_BOX_HEIGHT),
								   this, IDC ) ;
				pComboBox->SetFont( pCaptionFont ) ;
				CGroupParam *pGroupParam = (CGroupParam*)pParam ;
				int selected = 0 ;
				for ( size_t i=0 ; i<pGroupParam->values.size() ; i++ ){
					pComboBox->AddString( CString(pGroupParam->values[i].c_str()) ) ;
					if (pGroupParam->values[i]==pGroupParam->val) selected=int(i) ;
				}
				pComboBox->SetCurSel(selected) ;

			}
			break ;
		}
	}

	// Set the height change
	CRect pageRect ;
	GetWindowRect(&pageRect) ;
	m_dy = max( 0, m_dy-pageRect.Height() ) ;
	m_dx = max( 0, m_dx-pageRect.Width() ) ;

	// Set up the tooltip
	m_pToolTip = new CToolTipCtrl;
	if( m_pToolTip->Create(this) ){
		for ( size_t i=0 ; i<m_controls.size() ; i++ ){
			m_pToolTip->AddTool( m_controls[i].second, CString(m_params.members[i]->description.c_str()) ) ;
		}
		m_pToolTip->Activate(TRUE);
	}

	return TRUE;
}

void CPropertyPageSmall::OnBnClickedAccept()
{
	if (m_params.members.size() == m_controls.size()){
		for ( size_t i=0 ; i<m_params.members.size() ; i++ ){
			switch( m_params.members[i]->GetType() ){
				case CParam::INT:
					{
						CSliderGage *pSliderGage = (CSliderGage*)(m_controls[i].second) ;
						((CIntParam*)(m_params.members[i]))->val = pSliderGage->m_intVal ;
					}
					break ;
				case CParam::DBL:
					{
						CSliderGage *pSliderGage = (CSliderGage*)(m_controls[i].second) ;
						((CDoubleParam*)(m_params.members[i]))->val = pSliderGage->m_dblVal ;
					}
					break ;
				case CParam::STR:
					{
						CString str ;
						((CEdit*)(m_controls[i].second))->GetWindowText(str) ;
						char strMb[256] ;
						size_t c ;
						wcstombs_s( &c, strMb, sizeof(strMb), str.GetBuffer(), str.GetLength() ) ;
						strcpy(((CStringParam*)(m_params.members[i]))->val,strMb) ;
					}
					break ;
				case CParam::GRP:
					{
						CComboBox *pComboBox = (CComboBox*)(m_controls[i].second) ;
						int selIdx = pComboBox->GetCurSel() ;
						CString str ;
						pComboBox->GetLBText( selIdx, str ) ;
						char strMb[256] ;
						size_t c ;
						wcstombs_s( &c, strMb, sizeof(strMb), str.GetBuffer(), str.GetLength() ) ;
						((CGroupParam*)(m_params.members[i]))->val = string(strMb) ;
					}
					break ;
			}
		}
	}
	GetParentOwner()->PostMessageW( WM_COMMAND, CSetupDialogMFC::ACCEPT_PARAMS, NULL ) ;
}

BOOL CPropertyPageSmall::PreTranslateMessage(MSG* pMsg)
{
	if (NULL != m_pToolTip)
		m_pToolTip->RelayEvent(pMsg);

	return CPropertyPage::PreTranslateMessage(pMsg);
}


void CPropertyPageSmall::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CPropertyPage::OnShowWindow(bShow, nStatus);

	// Adjust the property pages
	CRect pageRect, buttonRect ;
	GetWindowRect(&pageRect) ;
	SetWindowPos( NULL, 0, 0, pageRect.Width()+m_dx, pageRect.Height()+m_dy, SWP_NOZORDER|SWP_NOMOVE ) ;
	CWnd *pWnd = GetDlgItem(IDC_ACCEPT) ;
	pWnd->GetWindowRect(&buttonRect) ;
	GetClientRect(&pageRect) ;
	pWnd->SetWindowPos( NULL, pageRect.Width()-buttonRect.Width()-6, pageRect.Height()-buttonRect.Height()-6, 0, 0, SWP_NOZORDER|SWP_NOSIZE ) ;
}


void CSetupDialogMFC::OnClose()
{
	m_pParentWindow->PostMessageW( WM_COMMAND, CLOSE_SETUP_WND, NULL ) ;
	
	// Uninstall the WH_GETMESSAGE hook function.
	VERIFY (::UnhookWindowsHookEx (hHook));

	CPropertySheet::OnClose();
}


BOOL CSetupDialogMFC::OnCommand(WPARAM wParam, LPARAM lParam)
{
	switch( wParam ){
	case CSetupDialogMFC::ACCEPT_PARAMS:
		m_pParentWindow->PostMessageW( WM_COMMAND, ACCEPT_PARAMS ) ;
		lParam = 0 ;
		break ;
	}

	return CPropertySheet::OnCommand(wParam, lParam);
}
