// SliderGage.cpp : implementation file
//

#include "stdafx.h"
#include "SliderGage.h"

#define WS_SUNKEN 0x00001000L

#define IDC_SLIDER 30000
#define SLIDER_SCALE 1000

// CSliderGage

IMPLEMENT_DYNAMIC(CSliderGage, CWnd)

CSliderGage::CSliderGage()
{

}

CSliderGage::~CSliderGage()
{
}


BEGIN_MESSAGE_MAP(CSliderGage, CWnd)
	ON_WM_CTLCOLOR()
	ON_WM_HSCROLL()
END_MESSAGE_MAP()



// CSliderGage message handlers

void CSliderGage::DrawControls()
{
	CRect rect ;
	GetClientRect( &rect ) ;
	int valWidth=60 ;
	int valHeight=17 ;

	// Slider
	int sliderLeft = 0 ;
	int sliderWidth = rect.right-sliderLeft ;
	m_slider.Create( WS_CHILD|BS_PUSHBUTTON|WS_VISIBLE,
					 CRect(sliderLeft,valHeight,sliderLeft+sliderWidth,rect.bottom), this, IDC_SLIDER ) ;
	if (m_isInteger){
		m_slider.SetRange( m_intMin, m_intMax ) ;
		m_slider.SetPos( m_intVal ) ;
	}
	else{
		if (m_dblMax > m_dblMin){
			m_slider.SetRange( 0, SLIDER_SCALE ) ;
			int pos = int((m_dblVal-m_dblMin)*SLIDER_SCALE/(m_dblMax-m_dblMin)) ;
			m_slider.SetPos( pos ) ;
		}
	}

	// min, val, and max values
	int minCapX = sliderLeft ;
	int valCapX = sliderLeft + (rect.right-sliderLeft-valWidth)/2 ;
	int maxCapX = rect.right - valWidth ;
	char str[128] ;
	CFont *pFont = GetParent()->GetFont() ;
	if (m_isInteger){
		sprintf( str, "%i", m_intMin ) ;
		m_minCap.CreateEx( WS_EX_LEFT, _T("STATIC"), CString(str), WS_CHILD|WS_VISIBLE|WS_SUNKEN, CRect(minCapX,0,minCapX+valWidth,valHeight), this, IDC_STATIC ) ;
		m_minCap.SetFont(pFont) ;
		sprintf( str, "%i", m_intVal ) ;
		m_valCap.CreateEx( WS_EX_LEFT, _T("STATIC"), CString(str), WS_CHILD|WS_VISIBLE|WS_SUNKEN, CRect(valCapX,0,valCapX+valWidth,valHeight), this, IDC_STATIC ) ;
		m_valCap.SetFont(pFont) ;
		sprintf( str, "%i", m_intMax ) ;
		m_maxCap.CreateEx( WS_EX_RIGHT, _T("STATIC"), CString(str), WS_CHILD|WS_VISIBLE|WS_SUNKEN, CRect(maxCapX,0,maxCapX+valWidth,valHeight), this, IDC_STATIC ) ;
		m_maxCap.SetFont(pFont) ;
	}
	else{
		sprintf( str, "%8.3lf", m_dblMin ) ;
		m_minCap.CreateEx( WS_EX_LEFT, _T("STATIC"), CString(str), WS_CHILD|WS_VISIBLE|WS_SUNKEN, CRect(minCapX,0,minCapX+valWidth,valHeight), this, IDC_STATIC ) ;
		m_minCap.SetFont(pFont) ;
		sprintf( str, "%8.3lf", m_dblVal ) ;
		m_valCap.CreateEx( WS_EX_LEFT, _T("STATIC"), CString(str), WS_CHILD|WS_VISIBLE|WS_SUNKEN, CRect(valCapX,0,valCapX+valWidth,valHeight), this, IDC_STATIC ) ;
		m_valCap.SetFont(pFont) ;
		sprintf( str, "%8.3lf", m_dblMax ) ;
		m_maxCap.CreateEx( WS_EX_RIGHT, _T("STATIC"), CString(str), WS_CHILD|WS_VISIBLE|WS_SUNKEN, CRect(maxCapX,0,maxCapX+valWidth,valHeight), this, IDC_STATIC ) ;
		m_maxCap.SetFont(pFont) ;
	}
}

HBRUSH CSliderGage::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CWnd::OnCtlColor(pDC, pWnd, nCtlColor);

	pDC->SetBkColor( RGB(255,255,255));

	// TODO:  Return a different brush if the default is not desired
	return hbr;
}

void CSliderGage::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	if ( nSBCode!=SB_THUMBPOSITION && nSBCode!=SB_ENDSCROLL ){
		if (*pScrollBar == m_slider){
			int pos = m_slider.GetPos() ;
			char str[256] ;
			if (m_isInteger){
				m_intVal = pos ;
				sprintf( str, "%i", m_intVal ) ;
				m_valCap.SetWindowText(CString(str)) ;
			}
			else{
				if (m_dblMax > m_dblMin){
					m_dblVal = m_dblMin + pos*(m_dblMax-m_dblMin)/SLIDER_SCALE ;
					sprintf( str, "%8.3lf", m_dblVal ) ;
					m_valCap.SetWindowText(CString(str)) ;
				}
			}
		}
	}

	CWnd::OnHScroll(nSBCode, nPos, pScrollBar);
}
