#pragma once


// CSliderGage

class CSliderGage : public CWnd
{
	DECLARE_DYNAMIC(CSliderGage)

public:
	CSliderGage();
	virtual ~CSliderGage();
	void DrawControls() ;

protected:
	DECLARE_MESSAGE_MAP()

public:
	bool m_isInteger ;
	int m_intMin, m_intMax, m_intVal ;
	double m_dblMin, m_dblMax, m_dblVal ;

protected:
	CStatic m_captionCap ;
	CStatic m_minCap ;
	CStatic m_valCap ;
	CStatic m_maxCap ;
	CSliderCtrl m_slider ;
public:
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
};


