// SetupDialog.h : main header file for the SetupDialog DLL
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CSetupDialogApp
// See SetupDialog.cpp for the implementation of this class
//

class CSetupDialogApp : public CWinApp
{
public:
	CSetupDialogApp();

// Overrides
public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
};
