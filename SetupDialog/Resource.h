//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by SetupDialog.rc
//
#define IDD_PROPPAGE_SMALL                102
#define IDC_BASE                        10000
#define IDC_PAGE_STEP                     100
#define IDC_ACCEPT                       1000

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        10001
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         10001
#define _APS_NEXT_SYMED_VALUE           10000
#endif
#endif
