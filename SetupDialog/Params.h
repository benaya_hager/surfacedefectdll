#pragma once

#include <stdio.h>
#include <io.h>
#include <direct.h>
#include <vector>
#include <string>
using namespace std;

class CParam
{
public:
	enum Type { INT, DBL, STR, GRP } ;

public:
	string name ;
	string group ;
	string description ;

public:
	virtual Type GetType() = 0 ;
};

class CIntParam : public CParam
{
public:
	int minVal, maxVal, val ;

public:
	Type GetType() { return INT; }

	bool Read( char *pBuf )
	{
		if (2 == sscanf( pBuf, "%i%i", &minVal, &maxVal )){
			char *pDscr1 = strchr(pBuf,'"') ;
			if (NULL != pDscr1){
				char *pDscr2 = strchr(pDscr1+1,'"') ;
				if (NULL != pDscr2){
					if (1 == sscanf( pDscr2+1, "%i", &val )){
						*pDscr2=NULL ;
						description = pDscr1+1 ;
						return true ;
					}
				}
			}
		}
		return false ;
	}
};

class CDoubleParam : public CParam
{
public:
	double minVal, maxVal, val ;

public:
	Type GetType() { return DBL; }
	
	bool Read( char *pBuf )
	{
		if (2 == sscanf( pBuf, "%lf%lf", &minVal, &maxVal )){
			char *pDscr1 = strchr(pBuf,'"') ;
			if (NULL != pDscr1){
				char *pDscr2 = strchr(pDscr1+1,'"') ;
				if (NULL != pDscr2){
					if (1 == sscanf( pDscr2+1, "%lf", &val)){
						*pDscr2=NULL ;
						description = pDscr1+1 ;
						return true ;
					}
				}
			}
		}
		return false ;
	}
};

class CStringParam : public CParam
{
public:
	//string val ; // causes problems when exported by a DLL
	char val[256] ;

public:
	Type GetType() { return STR; }

	bool Read( char *pBuf )
	{
		char *pDscr1 = strchr(pBuf,'"') ;
		if (NULL!=pDscr1){
			char *pDscr2 = strchr(pDscr1+1,'"') ;
			if (NULL!=pDscr2){
				char *pDscr3 = strchr(pDscr2+1,'"') ;
				if (NULL!=pDscr3){
					char *pDscr4 = strchr(pDscr3+1,'"') ;
					if (NULL!=pDscr4){
						*pDscr2=*pDscr4=NULL ;
						description = pDscr1+1 ;
						strcpy( val, pDscr3+1 ) ;
						return true ;
					}
				}
			}
		}
		return false ;
	}
};

class CGroupParam : public CParam
{
public:
	vector<string> values ;
	string val ;

public:
	~CGroupParam(){ values.clear(); }

	Type GetType() { return GRP; }

	bool Read( char *pBuf )
	{
		int count=0 ;
		values.clear() ;
		if ( 1==sscanf( pBuf, "%i", &count ) && count>0 ){
			int i ;
			for ( i=-1 ; i<count ; i++ ){
				char v[64] ;
				if (1 == sscanf( pBuf, "%s", v)){
					if (i>=0){
						values.push_back(v) ;
					}
				}
				else break ;
				pBuf += strlen(v)+1 ;
			}
			if (i == count){
				char *pDscr1 = strchr(pBuf,'"') ;
				if (NULL!=pDscr1){
					char *pDscr2 = strchr(pDscr1+1,'"') ;
					if (NULL!=pDscr2){
						char v[64] ;
						sscanf( pDscr2+1, "%s", v ) ;
						size_t j ;
						for ( j=0 ; j<values.size() ; j++ ){
							if (values[j].compare(v)==0) break ;
						}
						if (j < values.size()){
							val = v ;
							*pDscr2 = 0 ;
							description = pDscr1+1 ;
							return true ;
						}
					}
				}
			}
		}
		return false ;
	}
};


class CParamsGroup
{
public:
	string name ;
	vector<CParam*> members ;
};


class CParams
{
public:
	CParams(void) {} ;
	~CParams(void) { clear(); }

public:
	bool SetValue( string &groupName, string &paramName, string &description,
				   int minVal, int maxVal, int val )
	{
		// Search for a group with the given group name
		int gi, gs=int(groups.size()) ;
		for ( gi=0 ; gi<gs ; gi++ ){
			if (groups[gi].name==groupName) break ;
		}
		if (gi==gs){ // a new group needs to be added
			CParamsGroup group ;
			group.name = groupName ;
			groups.push_back(group) ;
		}

		// Search for a parameter with the given parameter name
		CParamsGroup &group   = groups[gi] ;
		int pi, ps=int(group.members.size()) ;
		for ( pi=0 ; pi<ps ; pi++ ){
			if ( group.members[pi]->name==paramName) break ;
		}
		if (pi==ps){ // a parameter by that name does not exist
			CIntParam *pParam = new CIntParam ;
			if (NULL!=pParam){
				pParam->name = paramName ;
				pParam->description = description ;
				pParam->minVal = minVal ;
				pParam->maxVal = maxVal ;
				pParam->val = val ;
				group.members.push_back(pParam) ;
				return true ;
			}
		}
		else{
			((CIntParam*)group.members[pi])->val = val ;
			return true ;
		}
		return false ;
	}

	bool SetValue( string &groupName, string &paramName, string &description,
				   double minVal, double maxVal, double val )
	{
		// Search for a group with the given group name
		int gi, gs=int(groups.size()) ;
		for ( gi=0 ; gi<gs ; gi++ ){
			if (groups[gi].name==groupName) break ;
		}
		if (gi==gs){ // a new group needs to be added
			CParamsGroup group ;
			group.name = groupName ;
			groups.push_back(group) ;
		}

		// Search for a parameter with the given parameter name
		CParamsGroup &group   = groups[gi] ;
		int pi, ps=int(group.members.size()) ;
		for ( pi=0 ; pi<ps ; pi++ ){
			if ( group.members[pi]->name==paramName) break ;
		}
		if (pi==ps){ // a parameter by that name does not exist
			CDoubleParam *pParam = new CDoubleParam ;
			if (NULL!=pParam){
				pParam->name = paramName ;
				pParam->description = description ;
				pParam->minVal = minVal ;
				pParam->maxVal = maxVal ;
				pParam->val = val ;
				group.members.push_back(pParam) ;
				return true ;
			}
		}
		else{
			((CDoubleParam*)group.members[pi])->val = val ;
			return true ;
		}
		return false ;
	}

	bool SetValue( string &groupName, string &paramName, string &description, string &val )
	{
		// Search for a group with the given group name
		int gi, gs=int(groups.size()) ;
		for ( gi=0 ; gi<gs ; gi++ ){
			if (groups[gi].name==groupName) break ;
		}
		if (gi==gs){ // a new group needs to be added
			CParamsGroup group ;
			group.name = groupName ;
			groups.push_back(group) ;
		}

		// Search for a parameter with the given parameter name
		CParamsGroup &group   = groups[gi] ;
		int pi, ps=int(group.members.size()) ;
		for ( pi=0 ; pi<ps ; pi++ ){
			if ( group.members[pi]->name==paramName) break ;
		}
		if (pi==ps){ // a parameter by that name does not exist
			CStringParam *pParam = new CStringParam ;
			if (NULL!=pParam){
				pParam->name = paramName ;
				pParam->description = description ;
				if (val.length()<sizeof(pParam->val)){
					strcpy(pParam->val,val.c_str()) ;
					group.members.push_back(pParam) ;
					return true ;
				}
			}
		}
		else{ // update the value of the parameter if the types and values match
			CStringParam *pParam = (CStringParam*)group.members[pi] ;
			if (val.length()<sizeof(pParam->val)){
				strcpy(pParam->val,val.c_str()) ;
				return true ;
			}
		}
		return false ;
	}

	bool SetValue( string &groupName, string &paramName, string &description, vector<string> &values, string &val )
	{
		// Search for a group with the given group name
		int gi, gs=int(groups.size()) ;
		for ( gi=0 ; gi<gs ; gi++ ){
			if (groups[gi].name==groupName) break ;
		}
		if (gi==gs){ // a new group needs to be added
			CParamsGroup group ;
			group.name = groupName ;
			groups.push_back(group) ;
		}

		// Search for a parameter with the given parameter name
		CParamsGroup &group   = groups[gi] ;
		int pi, ps=int(group.members.size()) ;
		for ( pi=0 ; pi<ps ; pi++ ){
			if ( group.members[pi]->name==paramName) break ;
		}
		if (pi==ps){ // a parameter by that name does not exist
			CGroupParam *pParam = new CGroupParam ;
			if (NULL!=pParam){
				pParam->name = paramName ;
				pParam->description = description ;
				pParam->values = values ;
				pParam->val = val ;
				group.members.push_back(pParam) ;
				return true ;
			}
		}
		else{ // update the value of the parameter if the types and values match
			CGroupParam *pParam = (CGroupParam*)group.members[pi] ;
			for ( size_t vi=0 ; vi<pParam->values.size() ; vi++ ){
				if (val==pParam->values[vi]){
					pParam->val = val ;
					return true ;
				}
			}
		}
		return false ;
	}

	void clear()
	{
		for ( size_t i=0 ; i<groups.size() ; i++ ){
			for ( size_t j=0 ; j<groups[i].members.size() ; j++ ){
				if (NULL != groups[i].members[j]){
					switch(groups[i].members[j]->GetType()){
					case CParam::INT:
					case CParam::DBL:
					case CParam::STR:
						delete groups[i].members[j] ;
						break ;
					case CParam::GRP:
						delete (CGroupParam*)(groups[i].members[j]) ;
						break ;
					}
				}
			}
			groups[i].members.clear() ;
		}
		groups.clear() ;
	}

	void Load( string &filename )
	{
		char buf[2048] ;
		
		// Clear all
		clear() ;

		// Read groups names
		GetPrivateProfileStringA( NULL, NULL, "", buf, sizeof(buf), filename.c_str() ) ;
		for ( char* pBuf=buf ; *pBuf!=0 ; ){
			CParamsGroup group ;
			group.name = pBuf ;
			pBuf += group.name.size()+1 ;
			groups.push_back(group) ;
		}

		// Read fields names
		for ( size_t i=0 ; i<groups.size() ; i++ ){
			GetPrivateProfileStringA( groups[i].name.c_str(), NULL, "", buf, sizeof(buf), filename.c_str() ) ;
			for ( char* pBuf=buf ; *pBuf!=0 ; ){
				CParam *pParam = new CIntParam ; // starting with an int param, which will be replaced later
				pParam->name = pBuf ;
				pBuf += pParam->name.size()+1 ;
				groups[i].members.push_back(pParam) ;
			}
		}

		// Read fields data
		for ( size_t i=0 ; i<groups.size() ; i++ ){
			for ( size_t j=0 ; j<groups[i].members.size() ; j++ ){
				CParam *&pParam = groups[i].members[j] ; 
				GetPrivateProfileStringA( groups[i].name.c_str(), pParam->name.c_str(), "", buf, sizeof(buf), filename.c_str() ) ;
				// Interpret field data
				char t[64], *pBuf ;
				if (1!=sscanf( buf, "%s", t ))
					continue ;
				pBuf=buf+strlen(t)+1 ;
				if (0==strcmp(t,"int")){
					CIntParam *pIntParam = (CIntParam*)pParam ;
					pIntParam->group = groups[i].name ;
					pIntParam->Read( pBuf ) ;
				}
				else if (0==strcmp(t,"double")){
					CDoubleParam *pDblParam = new CDoubleParam ;
					pDblParam->name = pParam->name ;
					pDblParam->group = groups[i].name ;
					delete pParam ;
					pParam = pDblParam ;
					pDblParam->Read( pBuf ) ;
				}
				else if (0==strcmp(t,"string")){
					CStringParam *pStrParam = new CStringParam ;
					pStrParam->name = pParam->name ;
					pStrParam->group = groups[i].name ;
					delete pParam ;
					pParam = pStrParam ;
					pStrParam->Read( pBuf ) ;
				}
				else if (0==strcmp(t,"group")){
					CGroupParam *pGroupParam = new CGroupParam ;
					pGroupParam->name = pParam->name ;
					pGroupParam->group = groups[i].name ;
					delete pParam ;
					pParam = pGroupParam ;
					pGroupParam->Read(pBuf) ;
				}
			}
		}
	}

	void Save( string &filename )
	{
		char buf[2048] ;

		// Groups loop
		for ( size_t i=0 ; i<groups.size() ; i++ ){
			// Group members loop
			for ( size_t j=0 ; j<groups[i].members.size() ; j++ ){
				CParam *&pParam = groups[i].members[j] ; 
				switch( pParam->GetType() ){
					case CParam::INT:
						{
							CIntParam *pIntParam = (CIntParam*)pParam ;
							sprintf( buf, "int %i %i \"%s\" %i",
									 pIntParam->minVal, pIntParam->maxVal,
									 pIntParam->description.c_str(),
									 pIntParam->val ) ;
						}
						break ;
					case CParam::DBL:
						{
							CDoubleParam *pDblParam = (CDoubleParam*)pParam ;
							sprintf( buf, "double %.4lf %.4lf \"%s\" %.4lf",
									 pDblParam->minVal, pDblParam->maxVal,
									 pDblParam->description.c_str(),
									 pDblParam->val ) ;
						}
						break ;
					case CParam::STR:
						{
							CStringParam *pStrParam = (CStringParam*)pParam ;
							sprintf( buf, "string \"%s\" \"%s\"",
									 pStrParam->description.c_str(),
									 pStrParam->val );
						}
						break ;
					case CParam::GRP:
						{
							CGroupParam *pGrpParam = (CGroupParam *)pParam ;
							sprintf( buf, "group %i ", int(pGrpParam->values.size()) ) ;
							for ( size_t i=0 ; i<pGrpParam->values.size() ; i++ ){
								strcat( buf, pGrpParam->values[i].c_str() ) ;
								strcat( buf, " " ) ;
							}
							sprintf( buf, "%s \"%s\" %s", buf, pGrpParam->description.c_str(), pGrpParam->val.c_str() ) ;
						}
						break ;
				}
				WritePrivateProfileStringA( groups[i].name.c_str(), pParam->name.c_str(), buf, filename.c_str() ) ;
			}
		}
	}

	void operator = (CParams &params)
	{
		clear() ;		
		for ( size_t gi=0 ; gi<params.groups.size() ; gi++ ){
			CParamsGroup group ;
			CParamsGroup &givenGroup = params.groups[gi] ;
			group.name = givenGroup.name ;
			for ( size_t mi=0 ; mi<givenGroup.members.size() ; mi++ ){
				CParam* givenParam = givenGroup.members[mi] ;
				switch(givenParam->GetType()){
					case CParam::INT:
						{
							CIntParam *pParam = new(CIntParam) ;
							*pParam = *(CIntParam*)givenParam ;
							group.members.push_back(pParam) ;
						}
						break ;
					case CParam::DBL:
						{
							CDoubleParam *pParam = new(CDoubleParam) ;
							*pParam = *(CDoubleParam*)givenParam ;
							group.members.push_back(pParam) ;
						}
						break ;
					case CParam::STR:
						{
							CStringParam *pParam = new(CStringParam) ;
							*pParam = *(CStringParam*)givenParam ;
							group.members.push_back(pParam) ;
						}
						break ;
					case CParam::GRP:
						{
							CGroupParam *pParam = new(CGroupParam) ;
							*pParam = *(CGroupParam*)givenParam ;
							group.members.push_back(pParam) ;
						}
						break ;
					default:
						continue ;
				}
			}
			groups.push_back(group) ;
		}
	}

	int GetIntValue( string &groupName, string &paramName )
	{
		int val=0 ;
		for ( size_t gi=0 ; gi<groups.size() ; gi++ ){
			if (groups[gi].name == groupName){
				for ( size_t mi=0 ; mi<groups[gi].members.size() ; mi++ ){
					if (groups[gi].members[mi]->name == paramName){
						if (groups[gi].members[mi]->GetType() ==  CParam::INT){
							CIntParam *pIntParam = (CIntParam*)(groups[gi].members[mi]) ;
							val = pIntParam->val ;
						}
					}
				}
			}
		}
		return val ;
	}

	int inline GetIntValue( char *groupName, char *paramName )
	{
		return GetIntValue( string(groupName), string(paramName) ) ;
	}

	double GetDoubleValue( string &groupName, string &paramName )
	{
		double val=0.0 ;
		for ( size_t gi=0 ; gi<groups.size() ; gi++ ){
			if (groups[gi].name == groupName){
				for ( size_t mi=0 ; mi<groups[gi].members.size() ; mi++ ){
					if (groups[gi].members[mi]->name == paramName){
						if (groups[gi].members[mi]->GetType() ==  CParam::DBL){
							CDoubleParam *pDoubleParam = (CDoubleParam*)(groups[gi].members[mi]) ;
							val = pDoubleParam->val ;
						}
					}
				}
			}
		}
		return val ;
	}

	double inline GetDoubleValue( char *groupName, char *paramName )
	{
		return GetDoubleValue( string(groupName), string(paramName) ) ;
	}

	string GetStringValue( string &groupName, string &paramName )
	{
		string val="" ;
		for ( size_t gi=0 ; gi<groups.size() ; gi++ ){
			if (groups[gi].name == groupName){
				for ( size_t mi=0 ; mi<groups[gi].members.size() ; mi++ ){
					if (groups[gi].members[mi]->name == paramName){
						if (groups[gi].members[mi]->GetType() == CParam::STR){
							CStringParam *pStringParam = (CStringParam*)(groups[gi].members[mi]) ;
							val = pStringParam->val ;
						}
						else if (groups[gi].members[mi]->GetType() == CParam::GRP){
							CGroupParam *pGroupParam = (CGroupParam*)(groups[gi].members[mi]) ;
							val = pGroupParam->val ;
						}
					}
				}
			}
		}
		return val ;
	}

	string inline GetStringValue( char *groupName, char *paramName )
	{
		return GetStringValue( string(groupName), string(paramName) ) ;
	}

public:
	vector<CParamsGroup> groups ;
};
