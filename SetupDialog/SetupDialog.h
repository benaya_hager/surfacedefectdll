#pragma once
#include "Params.h"

class CSetupDialog
{
public:
	AFX_API_EXPORT CSetupDialog(CWnd* pParent=NULL ) ;
	AFX_API_EXPORT ~CSetupDialog() ;
	AFX_API_EXPORT bool OpenDialog( bool modal=TRUE ) ;
	AFX_API_EXPORT void Close() ;
	AFX_API_EXPORT void AcceptParams() ;

private:
	CWnd* m_pParent ;
	CWnd *m_pDlg ;

public:
	enum { CLOSE_SETUP_WND=111111, ACCEPT_PARAMS } ;
	CParams m_params ;
};
