// SetupDialog.cpp : implementation file
//

#include "stdafx.h"
#include "SetupDialogMFC.h"
#include "SetupDialog.h"


AFX_API_EXPORT CSetupDialog::CSetupDialog(CWnd* pParent)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	m_pParent = pParent ;
	m_pDlg = NULL ;
}

AFX_API_EXPORT CSetupDialog::~CSetupDialog()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	if (m_pDlg){
		delete m_pDlg ;
		m_pDlg=NULL ;
	}
} ;

AFX_API_EXPORT bool CSetupDialog::OpenDialog( bool modal )
{
	bool rc=false ;
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	
	// Check parameters count. If m_params is empty notify and return false
	if (m_params.groups.size() == 0){
		m_pDlg->MessageBox( _T("Bad Setup File!!"), _T("Error"), MB_ICONHAND ) ;
		if (NULL!=m_pDlg){
			((CSetupDialog*)m_pDlg)->Close() ;
			m_pDlg = NULL ;
		}
		return false ;
	}

	// Close if already opened
	if (NULL!=m_pDlg){
		((CSetupDialog*)m_pDlg)->Close() ;
		m_pDlg = NULL ;
		rc = true ;
	}
	else{
		CSetupDialogMFC *pDlg = new CSetupDialogMFC( _T("Setup Dialog"), m_params, m_pParent ) ;
		if (NULL!=pDlg){
			if (modal){
				rc = (IDOK == pDlg->DoModal()) ;
				m_params = pDlg->m_params ;
				delete pDlg ;
			}
			else{
				m_pDlg = (CWnd*)pDlg ;
				pDlg->Create() ;
				pDlg->ShowWindow(SW_SHOW) ;
				rc = true ;
			}
		}
	}
	return rc ;
}

AFX_API_EXPORT void CSetupDialog::Close()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	if (NULL!=m_pDlg){
		CSetupDialogMFC* pDlg = (CSetupDialogMFC*)m_pDlg ;
		m_params = pDlg->m_params ;
		pDlg->DestroyWindow() ;
	}
}

void CSetupDialog::AcceptParams()
{
	if (NULL!=m_pDlg)
		m_params=((CSetupDialogMFC*)m_pDlg)->m_params ;
}
