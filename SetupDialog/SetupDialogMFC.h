#pragma once

#include "resource.h"
#include <vector>
#include "params.h"


// CPropertyPageSmall dialog

class CPropertyPageSmall : public CPropertyPage
{
	DECLARE_DYNAMIC(CPropertyPageSmall)

public:
	CPropertyPageSmall();
	virtual ~CPropertyPageSmall();

	CPropertyPageSmall(const CPropertyPageSmall& src) {memcpy( this, &src, sizeof(src) );}
	void operator=(const CPropertyPageSmall& src) {memcpy( this, &src, sizeof(src) );}
	
// Dialog Data
	enum { IDD = IDD_PROPPAGE_SMALL };

public:
	int m_index ;
	CParamsGroup m_params ;
	vector<pair<CStatic*,CWnd*>> m_controls ;
	CToolTipCtrl* m_pToolTip;
	int m_dx, m_dy ;
	int m_captionWidth, m_captionHeight ;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedAccept();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
};


// CSetupDialogMFC

class CSetupDialogMFC : public CPropertySheet
{
	DECLARE_DYNAMIC(CSetupDialogMFC)

public:
	CSetupDialogMFC(LPCTSTR pszCaption, CParams &params, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	virtual ~CSetupDialogMFC();

public:
	enum { CLOSE_SETUP_WND=111111, ACCEPT_PARAMS } ;
	std::vector<CPropertyPageSmall*> m_propPages ;
	CParams m_params ;
	CWnd *m_pParentWindow ;
	int m_captionWidth ;
	int m_captionHeight ;

protected:
	void AddSetupPages() ;
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	virtual BOOL OnInitDialog();
	afx_msg void OnClose();
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
};
