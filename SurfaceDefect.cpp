// SurfaceDefect.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "direct.h"
#include "SurfaceDefect.h"
#include "opencv2\highgui\highgui.hpp"
#include "opencv2\core\core.hpp"
#include "opencv2\imgproc\imgproc.hpp"
using namespace cv;

#include <vector>
using namespace std;


class CDetector
{
public:
	CDetector() {}
	~CDetector() {}

	void SetParams( bool saveIntermediate,
					int filterSize,
					double maxDist,
					double minCurvature,
					double thresholdRatio,
					int wideWidthThr,
					int desiredWidePeaksCount )
	{
		m_saveIntermediate = saveIntermediate ;
		m_filterSize = filterSize ;
		m_maxDist = maxDist ;
		m_minCurvature = minCurvature ;
		m_thresholdRatio = thresholdRatio ;
		m_wideWidthThr = wideWidthThr ;
		m_desiredWidePeaksCount = desiredWidePeaksCount ;
	}

	/*
	bool Detect( Mat &inputImage, vector<pair<Point,Point>> &lines, Mat &resImage, vector<int> &badLineIndexes )
	*/
	bool Detect( Mat &inputImage, vector<pair<Point,Point>> &lines, vector<int> &badLineIndexes )
	{
		bool rc=true ;
		// Color definitions
		Scalar blue(255,0,0), darkBlue(192,0,0),
			   green(0,255,0), darkGreen(0,192,0),
			   red(0,0,255), darkRed(0,0,192),
			   magenta(255,0,255), darkMagenta(192,0,192),
			   cyan(255,255,0), darkCyan(192,192,0),
			   yellow(0,255,255), darkYellow(0,192,192) ;

		// Prepare a gray image for processing
		Mat greyImage ;
		if (3==inputImage.channels()) cvtColor( inputImage, greyImage, CV_BGR2GRAY ) ;
		else inputImage.copyTo(greyImage) ;

		// Get the lines vector
		vector<vector<uchar>> linesValues ;
		CollectLinesValues( greyImage, lines, linesValues ) ;
		for ( size_t il=0 ; il<linesValues.size() ; il++ ){
			// try to find two peaks in the line profile
			vector<int> peaksIdxes ;
			int minVal, maxVal ;
			double thrVal ;
			bool wideLine ;
			FindPeaks( linesValues[il], minVal, maxVal, thrVal, wideLine, peaksIdxes ) ;

			// If two peaks found the tested profile is ok
			/*
			vector<Point>defectPoints ;
			*/
			if ( !(wideLine && peaksIdxes.size()>=m_desiredWidePeaksCount) && 
				 !(!wideLine && peaksIdxes.size()>=1) )
			{
				/*
				// Get the center point
				pair<Point,Point> &currLine = lines[il] ;
				Point currPoint = Point( (currLine.first.x+currLine.second.x)/2,
					(currLine.first.y+currLine.second.y)/2) ;

				// collect the bad point
				defectPoints.push_back(currPoint) ;
				*/
				badLineIndexes.push_back(int(il)) ;
			}
		}
		
		/*
		// defect points indication
		int crossSize=5 ;
		for ( size_t i=0 ; i<defectPoints.size() ; i++ ){
			Point &p = defectPoints[i] ;
			line( resImage, Point(p.x-crossSize,p.y), Point(p.x+crossSize,p.y), red ) ;
			line( resImage, Point(p.x,p.y-crossSize), Point(p.x,p.y+crossSize), red ) ;
		}

		if (m_saveIntermediate){
			imwrite( "DefectImage.png", resImage ) ;
		}
		*/

		return rc ;
	}

	void FindPeaks( vector<uchar> &lineValues,
					int &minVal,
					int &maxVal,
					double &thrVal,
					bool &wideLine,
					vector<int> &peakIdxes )
	{
		peakIdxes.clear() ;

		// Find parabolic coefficients
		static Mat aFilter, bFilter, cFilter ;
		static int oldFilterSize=-1 ;
		if (oldFilterSize!=m_filterSize){
			GetParabolicFitCoeficients( aFilter, bFilter, cFilter ) ;
			oldFilterSize = m_filterSize ;
		}

		// Apply the filters
		vector<double> aRes, bRes, cRes ;
		Convolve( lineValues, aFilter, aRes ) ;
		Convolve( lineValues, bFilter, bRes ) ;
		Convolve( lineValues, cFilter, cRes ) ;

		// Search for two peaks
		vector<pair<int,double>> peaks ;
		FindAllPeaks( aRes, bRes, cRes, peaks ) ;
		SelectEdgePeaks( lineValues, minVal, maxVal, thrVal, wideLine, peaks ) ;

		// Collect the indexes of the found peaks
		for ( size_t i=0 ; i<peaks.size() ; peakIdxes.push_back(peaks[i++].first) ) ;
	}

	void CollectSingleLineValues( Mat &inputImage, pair<Point,Point> &line, vector<uchar> &lineValues )
	{
		lineValues.clear() ;
		if (inputImage.empty()) return ;
		int step = int(inputImage.step) ;
		Point p1=line.first, p2=line.second ;
		int dx=p2.x-p1.x, dy=p2.y-p1.y ;
		int adx=abs(dx), ady=abs(dy) ;
		if (adx>0 || ady>0){
			int i, di, n ;
			double sum, r, f ;

			if (adx >= ady){
				di = 2*(dx > 0.0) - 1 ;
				r = double(dy)/adx ;
				for ( i=p1.x, f=p1.y, n=0, sum=0.0 ; i!=p2.x ; i+=di, f+=r, n++ ){
					uchar *p = (uchar*)(inputImage.data) + step*int(f+0.5)+i ;
					lineValues.push_back(*p) ;
				}
			}
			else{
				di = 2*(dy > 0.0) - 1 ;
				r = double(dx)/ady ;
				for ( i=p1.y, f=p1.x, n=0, sum=0.0 ; i!=p2.y ; i+=di, f+=r, n++ ){
					uchar *p = (uchar*)(inputImage.data) + step*i+int(f+0.5) ;
					lineValues.push_back(*p) ;
				}
			}
		}
	}

	void GetParabolicParams( vector<uchar> &profile, int profileIdx, double &a, double &b, double &c )
	{
		a = b = c = 0.0 ;
		if ( profileIdx>=m_filterSize/2 && profileIdx+m_filterSize/2<profile.size() ){

			// Find parabolic coefficients
			//static int filterSize ;
			static Mat aFilter, bFilter, cFilter ;
			GetParabolicFitCoeficients( aFilter, bFilter, cFilter ) ;

			// Apply the filters
			vector<double> aRes, bRes, cRes ;
			Convolve( profile, aFilter, aRes ) ;
			Convolve( profile, bFilter, bRes ) ;
			Convolve( profile, cFilter, cRes ) ;

			// Get the values at the desired index
			a=aRes[profileIdx], b=bRes[profileIdx], c=cRes[profileIdx] ;
		}
	}

private:
	void CollectLinesValues( Mat &inputImage, vector<pair<Point,Point>> &lines, vector<vector<uchar>> &linesValues )
	{
		size_t width=inputImage.cols, height=inputImage.rows, step=inputImage.step ;
		linesValues.clear() ;

		for ( size_t li=0 ; li<lines.size() ; li++ ){
			vector<uchar> lineValues ;
			CollectSingleLineValues( inputImage, lines[li], lineValues ) ;
			if (lineValues.size() > 0){
				linesValues.push_back(lineValues) ;
			}
		}
	}

	void GetParabolicFitCoeficients( Mat &aFilter, Mat &bFilter, Mat &cFilter )
	{
		int m = int(m_filterSize/2) ;
		// Calculate the matrix A
		//      _              _     _                                                        _
		//     | Sx^4 Sx^3 Sx^2 |   | m(m+1)(2m+1)(3m^2+3m-1)/15       0        m(m+1)(2m+1)/3 |
		//     |                |   |                                                          |
		// A = | Sx^3 Sx^2 Sx   | = |              0             m(m+1)(2m+1)/3       0        |
		//     |                |   |                                                          |
		//     |_Sx^2 Sx   2m+1_|   |_       m(m+1)(2m+1)/3            0             2m+1     _|
		Mat A(3,3,CV_64F) ;
		double m1=m+1 ;
		double _2m1=2*m+1 ;
		A.at<double>(0,0)=m*m1*_2m1*(3*(m*m+m)-1)/15.0 ;
		A.at<double>(0,1)=0.0 ;
		A.at<double>(0,2)=m*m1*_2m1/3.0 ;
		A.at<double>(1,0)=0.0 ;
		A.at<double>(1,1)=A.at<double>(0,2) ;
		A.at<double>(1,2)=0.0 ;
		A.at<double>(2,0)=A.at<double>(0,2) ;
		A.at<double>(2,1)=0.0 ;
		A.at<double>(2,2)=_2m1 ;

		// Calculate the matrix S
		//      _                                              _
		//     | (-m)^2   (-(m-1)^2  ...  0  ...  (m-1)^2   m^2 |
		//     |                                                |
		// S = | -m       -(m-1)     ...  0  ...  m-1       m   |
		//     |                                                |
		//     |_1        1          ...  1  ...  1         1  _|
		Mat S(3,int(_2m1),CV_64F) ;
		for ( int i=-m ; i<=m ; i++ ){
			S.at<double>(0,i+m) = i*i ;
			S.at<double>(1,i+m) = i ;
			S.at<double>(2,i+m) = 1 ;
		}

		//                                                 -1
		// Calculate the 3 filters which are the rows of  A * S
		Mat iA ;
		double r = invert( A, iA ) ;
		Mat F = iA*S ;
		aFilter = F.row(0) ;
		bFilter = F.row(1) ;
		cFilter = F.row(2) ;
	}

	void Convolve( vector<uchar> &lineValues, Mat &filter, vector<double> &resValues )
	{
		int fSize=filter.cols, m=fSize/2, dSize=int(lineValues.size()) ;
		resValues = vector<double>(dSize,0.0) ;
		for ( int i=m ; i<dSize-m ; i++ ){
			double sum=0.0 ;
			for ( int j=-m, k=0, l=i+j ; j<=m ; j++, k++, l++ ){
				sum += filter.at<double>(k)*lineValues[l] ;
			}
			resValues[i] = sum ;
		}
	}

	void FindAllPeaks( vector<double> &aValues, vector<double> &bValues, vector<double> &cValues,
					   vector<pair<int,double>> &peaks )
	{
		peaks.clear() ;
		int size=int(aValues.size()) ;
		if (size>5 && size==bValues.size() && size==cValues.size()){
			for ( int i=0 ; i<size ; i++ ){
				double a=aValues[i], b=bValues[i] ;
				if (-a > m_minCurvature){ // 1st peak condition
					double peakDist = abs(b/(2*a)) ;
					if (peakDist < m_maxDist){ // 2nd peak condition
						peaks.push_back(pair<int,double>(i,cValues[i])) ;
					}
				}
			}
		}
	}

	void SelectEdgePeaks( vector<uchar> &lineValues,
						  int &minVal,
						  int &maxVal,
						  double &thrVal,
						  bool &wideLine,
						  vector<pair<int,double>> &peaks )
	{
		// Find the minimal and maximal values
		minVal=255, maxVal=0 ;
		for ( size_t i=0 ; i<lineValues.size() ; i++ ){
			int val=int(lineValues[i]) ;
			if (minVal>val) minVal=val ;
			if (maxVal<val) maxVal=val ;
		}
		if (minVal>maxVal) { peaks.clear(); return; }

		// Calculate the threshold
		thrVal = minVal + m_thresholdRatio*(maxVal-minVal) ; // Find values threshold

		// Calculate line width
		size_t i1, i2 ;
		for ( i1=0 ; i1<lineValues.size() ; i1++ ){
			if (lineValues[i1]>thrVal) break ;
		}
		for ( i2=lineValues.size()-1 ; i2>0 ; i2-- ){
			if (lineValues[i2]>thrVal) break ;
		}

		// Decide if the line is wide or narrow
		wideLine = (i2-i1 > m_wideWidthThr ) ;

		// Delete peaks with values less than the threshold
		for ( int i=int(peaks.size())-1 ; i>=0 ; i-- ){
			if (peaks[i].second < thrVal) peaks.erase( peaks.begin()+i ) ;
		}

		if ( wideLine && peaks.size()>=m_desiredWidePeaksCount ){
			// Split the peaks to left-peaks and right-peaks.
			// The split is around the center index.
			double center = 0.5*lineValues.size() ;
			vector<pair<int,double>> leftPeaks, rightPeaks, *peaksArr[2]={&leftPeaks,&rightPeaks} ;
			for ( size_t i=0 ; i<peaks.size() ; i++ ){
				peaksArr[(double(peaks[i].first) >= center)]->push_back(peaks[i]) ;
			}

			// Select the element with highest value (second) in the left-peaks, and the right-peaks
			peaks.clear() ;
			double maxVal=0.0 ;
			int iMax=-1 ;
			for ( size_t i=0 ; i<leftPeaks.size() ; i++ ){
				if (maxVal < leftPeaks[i].second) ( maxVal=leftPeaks[i].second, iMax=int(i) ) ;
			}
			if (iMax>-1)
				peaks.push_back(leftPeaks[iMax]) ;
			maxVal=0.0 ;
			iMax=-1 ;
			for ( size_t i=0 ; i<rightPeaks.size() ; i++ ){
				if (maxVal < rightPeaks[i].second) ( maxVal=rightPeaks[i].second, iMax=int(i) ) ;
			}
			if (iMax>-1)
				peaks.push_back(rightPeaks[iMax]) ;
		}
		else if ( !wideLine && peaks.size()>0 ){
			if (peaks.size()>1){// Keep only the highest peak
				double maxVal=0.0 ;
				int iMax=-1 ;
				for ( int i=0 ; i<int(peaks.size()) ; i++ ){
					if (maxVal < peaks[i].second){
						maxVal = peaks[i].second ;
						if (iMax>-1){
							peaks.erase( peaks.begin()+iMax ) ;
							i-- ;
						}
						iMax=i ;
					}
					else{
						peaks.erase( peaks.begin()+i ) ;
						i-- ;
					}
				}
			}
		}
	}
	
protected:
	bool m_saveIntermediate ;
	int m_filterSize ;
	double m_maxDist ;
	double m_minCurvature ;
	double m_thresholdRatio ;
	int m_wideWidthThr ;
	int m_desiredWidePeaksCount ;

};

CDetector detector ;



// API Functions
// =============

// InitDetector starts the mouse-on-shelf detection process.
DETECTOR_API bool InitDetector( bool saveIntermediate,
								int filterSize,
								double maxDist,
								double minCurvature,
								double thresholdRatio,
								int wideWidthThr,
								int desiredWidePeaksCount )
{
	detector.SetParams( saveIntermediate,
						filterSize,
						maxDist,
						minCurvature,
						thresholdRatio,
						wideWidthThr,
						desiredWidePeaksCount ) ;
	return true ;
}		

// The Detect function runs the detection function on a given image, collects and reports the results.
DETECTOR_API bool Detect( char *imageData,
						  int width,
						  int height,
						  int channels,
						  int *pLines,
						  int lineCount,
						  int *pBadLineIndexes,
						  int &badCount )
{
	if ( NULL==imageData || 0>=width || 0>=height )
		return false ;;

	// Get a Mat image
	Mat inColor, inGray ;
	if (3==channels){
		inColor = Mat(height,width,CV_8UC3,imageData) ;
		cvtColor( inColor, inGray, CV_BGR2GRAY ) ;
	}
	else if (1==channels){
		inGray = Mat(height,width,CV_8UC1,imageData) ;
		cvtColor( inGray, inColor, CV_GRAY2BGR ) ;
	}
	else
		return false ;

	// Get the lines vector
	if ( NULL==pLines || lineCount<1 )
		return false ;
	vector<pair<Point,Point>> lines(lineCount) ;
	for ( int i=0 ; i<lineCount ; i++ ){
		int j=4*i ;
		lines[i] = pair<Point,Point>(Point(pLines[j],pLines[j+1]),
									 Point(pLines[j+2],pLines[j+3])) ;
	}

	if ( NULL==pBadLineIndexes )
		return false ;
	vector<int> badLineIndexes ;
	bool rc = detector.Detect( inGray, lines, badLineIndexes ) ;
	badCount = min( badCount, int(badLineIndexes.size()) ) ;
	for ( int i=0 ; i<badCount ; i++ ){
		pBadLineIndexes[i] = badLineIndexes[i] ;
	}

	return rc ;
}

DETECTOR_API void FindPeaks( unsigned char *pLineValues,
							 int lineValuesSize,
							 int &minVal,
							 int &maxVal,
							 double &thrVal,
							 bool &wideLine,
							 int *pPeakIdxes,
							 int &peakIdxesCount )
{
	if (lineValuesSize<=0 )
		return ;
	vector<uchar> lineValues(lineValuesSize) ;
	vector<int> peaksIdxes ;
	for ( int i=0 ; i<lineValuesSize ; i++ )
		lineValues[i] = uchar(pLineValues[i]) ;
	detector.FindPeaks( lineValues, minVal, maxVal, thrVal, wideLine, peaksIdxes ) ;
	peakIdxesCount = min( 2, int(peaksIdxes.size()) ) ;
	for ( int i=0 ; i<peakIdxesCount ; i++ )
		pPeakIdxes[i] = peaksIdxes[i] ;
}

DETECTOR_API void CollectSingleLineValues( char *imageData, int width, int height, int channels,
										   int *pLine, unsigned char *pLineValues, int &lineValuesCount )
{
	if ( NULL==imageData || 0>=width || 0>=height ||
		 NULL==pLine || NULL==pLineValues || lineValuesCount<=0 ) return ;;

	// Get a Mat image
	Mat inGray ;
	if (3==channels){
		Mat inColor = Mat(height,width,CV_8UC3,imageData) ;
		cvtColor( inColor, inGray, CV_BGR2GRAY ) ;
	}
	else if (1==channels){
		inGray = Mat(height,width,CV_8UC1,imageData) ;
	}
	else return ;

	vector<uchar> lineValues ;
	detector.CollectSingleLineValues( inGray,
									  pair<Point,Point>(Point(pLine[0],pLine[1]),Point(pLine[2],pLine[3])),
									  lineValues ) ;
	lineValuesCount = min( lineValuesCount, int(lineValues.size()) ) ;
	for ( int i=0 ; i<lineValuesCount ; i++ )
		pLineValues[i] = lineValues[i] ;
}

DETECTOR_API void GetParabolicParams( unsigned char *profileData, int &profileDataSize,
									  int profileIdx, double &a, double &b, double &c )
{
	if ( NULL==profileData || profileDataSize<=0 )
		return ;
	vector<unsigned char> profile(profileDataSize) ;
	for ( int i=0 ; i<profileDataSize ; i++ )
		profile[i]=profileData[i] ;
	detector.GetParabolicParams( profile, profileIdx, a, b, c ) ;
}
