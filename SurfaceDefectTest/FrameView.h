#pragma once

#include <vector>
#include <algorithm>
#include <string>

using namespace std;

/*
#define emptyRect CRect(0,0,0,0)
#define isLegalRect(r) ((r).left>0 && (r).left<(r).right && (r).top>0 && (r).top<(r).bottom)
#define emptyPoint CPoint(0,0)
#define isLegalPoint(p) ((p).x>0 && (p).y>0)
#define unchangedRect CRect(-1,-1,-1,-1)
#define unchangedPoint CPoint(-1,-1)
#define IgnoreRectInput(r) ((-1==(r).left)&&(-1==(r).right)&&(-1==(r).top)&&(-1==(r).bottom))
#define IgnorePointInput(p) ((-1==(p).x)&&(-1==(p).y))
*/

// CFrameView

class CFrameView : public CWnd
{
	DECLARE_DYNAMIC(CFrameView)

public:
	CFrameView();
	virtual ~CFrameView();
	void SetImage( char *data, int width, int height, int depth ) ;
	void SetScales() ;

public:
	enum { START_MOVE=WM_USER+100, PIX_DATA, SELECT_POS } ;
	int m_xPix, m_yPix ;

private:
	bool TranslateToPixels( CPoint &point ) ;

	void *m_imageBuff ;
	double m_xScale, m_yScale ;
	CPoint m_firstPos, m_lastPos ;

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnSize(UINT nType, int cx, int cy);
};


