// FrameView.cpp : implementation file
//

#include "stdafx.h"
#include <math.h>
#include "GraphicView.h"


#define emptyRect CRect(0,0,0,0)
#define isLegalRect(r) ((r).left>0 && (r).left<(r).right && (r).top>0 && (r).top<(r).bottom)
#define emptyPoint CPoint(0,0)
#define isLegalPoint(p) ((p).x>0 && (p).y>0)
#define unchangedRect CRect(-1,-1,-1,-1)
#define unchangedPoint CPoint(-1,-1)
#define IgnoreRectInput(r) ((-1==(r).left)&&(-1==(r).right)&&(-1==(r).top)&&(-1==(r).bottom))
#define IgnorePointInput(p) ((-1==(p).x)&&(-1==(p).y))


IMPLEMENT_DYNAMIC(CGraphicView, CWnd)

CGraphicView::CGraphicView()
	: m_firstPos(CPoint(0,0))
	, m_lastPos(CPoint(0,0))
{
}

CGraphicView::~CGraphicView()
{
}


BEGIN_MESSAGE_MAP(CGraphicView, CWnd)
	ON_WM_PAINT()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_SIZE()
	ON_WM_LBUTTONDBLCLK()
END_MESSAGE_MAP()



// CGraphicView message handlers


void CGraphicView::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	/*
	RECT windowRect, clientRect ;
	GetWindowRect( &windowRect ) ;
	ScreenToClient(&windowRect) ;
	*/
	RECT clientRect ;
	GetClientRect(&clientRect) ;
	CRgn rgn ;
	rgn.CreateRectRgn( clientRect.left, clientRect.top, clientRect.right, clientRect.bottom ) ;
	dc.SelectClipRgn( &rgn ) ;

	CDC *pDC = GetDC() ;
	if ( NULL!=pDC && NULL!=pDC->m_hDC ){
		int profileSize=int(m_profile.size()) ;
		int bottom=clientRect.bottom, right=clientRect.right ;
		// Draw a profile, limits and threshold lines
		if (profileSize > 2){
			CBrush whiteBrush(0x00ffffff) ;
			CPen cyanPen(PS_SOLID,1,0x00c0c000) ;
			CPen magentaPen(PS_SOLID,1,0x00ff00ff) ;
			CPen bluePen(PS_SOLID,1,0x00ff0000) ;
			CPen redPen(PS_SOLID,1,0x000000ff) ;

			// Clear the window
			pDC->FillRgn(&rgn,&whiteBrush) ;

			// Draw max, min and thr lines
			//int maxVal=200, minVal=25 ;
			pDC->SelectObject(&cyanPen);
			pDC->MoveTo(0,bottom-m_minVal/2) ;
			pDC->LineTo(right,bottom-m_minVal/2) ;
			pDC->MoveTo(0,bottom-m_maxVal/2) ;
			pDC->LineTo(right,bottom-m_maxVal/2) ;
			pDC->SelectObject(&magentaPen);
			pDC->MoveTo(0,bottom-int(m_thrVal/2)) ;
			pDC->LineTo(right,bottom-int(m_thrVal/2)) ;

			// Draw profile line
			pDC->SelectObject(&bluePen) ; 
			pDC->MoveTo(0,bottom-m_profile[0]/2) ;
			for ( int i=1 ; i<profileSize ; i++ ){
				pDC->LineTo(3*i,bottom-m_profile[i]/2) ;
			}

			// Write limits, threshold, average and median values
			CFont font;
			VERIFY(font.CreateFont( 12,                        // nHeight
								    0,                         // nWidth
								    0,                         // nEscapement
								    0,                         // nOrientation
								    FW_NORMAL,                 // nWeight
								    FALSE,                     // bItalic
								    FALSE,                     // bUnderline
								    0,                         // cStrikeOut
								    ANSI_CHARSET,              // nCharSet
								    OUT_DEFAULT_PRECIS,        // nOutPrecision
								    CLIP_DEFAULT_PRECIS,       // nClipPrecision
								    DEFAULT_QUALITY,           // nQuality
								    DEFAULT_PITCH | FF_SWISS,  // nPitchAndFamily
								    _T("Arial")));                 // lpszFacename
			CFont* def_font = pDC->SelectObject(&font);
			font.DeleteObject();
			CString txt ;

			pDC->SetTextColor(0x0000c000) ;
			txt.Format(_T("avr %.1f"),m_avrgVal) ;
			pDC->TextOutW( 5, max(0,int(0.5+bottom-m_avrgVal/2-15)), txt ) ;
			pDC->SetTextColor(0x00c000c0) ;
			txt.Format(_T("med %.1f"),m_medVal) ;
			pDC->TextOutW( 5, max(0,int(0.5+bottom-m_medVal/2-15)), txt ) ;

			pDC->SetTextColor(0x00c0c000) ;
			txt.Format(_T("%i"),m_maxVal) ;
			pDC->TextOutW( right-25, max(0,bottom-m_maxVal/2-15), txt ) ;
			txt.Format(_T("%i"),m_minVal) ;
			pDC->TextOutW( right-25, max(0,bottom-m_minVal/2-15), txt ) ;
			pDC->SetTextColor(0x00ff00ff) ;
			txt.Format(_T("%.1f"),m_thrVal) ;
			pDC->TextOutW( right-25, max(0,int(0.5+bottom-m_thrVal/2-15)), txt ) ;

			// Write profile center position
			pDC->SetTextColor(0x00000000) ;
			//txt.Format(_T("Profile pos. (%i,%i)"),m_centerLocal.x,m_centerLocal.y) ;
			txt.Format(_T("Profile pos. local(%i,%i) global(%i,%i)"),m_centerLocal.x,m_centerLocal.y,m_centerGlobal.x,m_centerGlobal.y) ;
			pDC->TextOutW( 5, bottom-12, txt ) ;

			// Draw peaks
			int len=5 ;
			pDC->SelectObject(&redPen) ;
			pDC->SetTextColor(0x000000ff) ;
			for ( size_t i=0 ; i<m_peaksIdxes.size() ; i++ ){
				int idx = m_peaksIdxes[i] ;
				int val = m_profile[idx] ;
				int x = 3*idx ;
				int y = bottom-val/2 ;
				pDC->MoveTo(x-len,y) ;
				pDC->LineTo(x+len,y) ;
				pDC->MoveTo(x,y-len) ;
				pDC->LineTo(x,y+len) ;
				txt.Format(_T("(%i,%i)"),idx,val) ;
				pDC->TextOutW(x+len,y-15,txt) ;
			}
			pDC->SelectObject(def_font) ;
		}
		ReleaseDC(pDC) ;
	}
}

void CGraphicView::OnLButtonDown(UINT nFlags, CPoint point)
{
	if (isLegalPoint(point)){
		m_firstPos = point ;
		if (MK_CONTROL&nFlags){ // LEFT_DOWN_CONTROL
			RECT clientRect ;
			GetClientRect(&clientRect) ;
			m_point.x = point.x/3 ;
			m_point.y = 2*(clientRect.bottom - point.y) ;
			::PostMessage( GetParent()->m_hWnd, WM_COMMAND, SELECT_POS, NULL ) ;//(LPARAM)this ) ;
		}
		else if (MK_SHIFT&nFlags){ // LEFT_DOWN_SHIFT
		}
		else{
		}
	}

	CWnd::OnLButtonDown(nFlags, point);
}

void CGraphicView::OnLButtonUp(UINT nFlags, CPoint point)
{
	m_firstPos = m_lastPos = CPoint(0,0) ;
	CWnd::OnLButtonUp(nFlags, point);
}

void CGraphicView::OnMouseMove(UINT nFlags, CPoint point)
{
	if (isLegalPoint(point)){
		if (nFlags&MK_LBUTTON){
			m_lastPos = point ;
		}
		else if (nFlags==0){
			Invalidate() ;
			//::PostMessage( GetParent()->m_hWnd, WM_COMMAND, PIX_DATA, (LPARAM)this ) ;
		}
	}
	CWnd::OnMouseMove(nFlags, point);
}

void CGraphicView::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);
	SetScales() ;
}

void CGraphicView::SetScales()
{
}

