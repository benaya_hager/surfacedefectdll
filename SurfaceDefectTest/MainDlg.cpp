
// MainDlg.cpp : implementation file
//

#include "stdafx.h"

#include <Params.h>
#include <SetupDialog.h>
#include "SurfaceDefectTest.h"
#include "MainDlg.h"
#include "afxdialogex.h"

#include <SurfaceDefect.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


void ReadLines( string &linesFileName, Rect &roi, vector<pair<Point,Point>> &lines )
{
	lines.clear() ;
	if ( roi.width<=0 || roi.height<=0 || roi.x<0 || roi.y<0 )
		return ;
	FILE *fp = fopen( linesFileName.c_str(), "rt" ) ;
	if (NULL==fp)
		return ;
	int l=roi.x, t=roi.y, r=l+roi.width, b=t+roi.height ;
	int x1, y1, x2, y2 ;
	bool rotate=true ;
	int mode=0 ; // 0 - original, 1 - length doubling, 2 - 90� rotation, 3 - 90� rotation and length doubling
	while( 4 == fscanf(fp,"%i,%i,%i,%i\n",&x1,&y1,&x2,&y2) ){

		// Rotate the line 90around its center (to fix Roee's bug)
		int sx=x1+x2, dx=x1-x2, sy=y1+y2, dy=y1-y2 ;
		switch(mode)
		{
		case 1:
			// doubling the length
			x1 += dx/2 ;
			y1 += dy/2 ;
			x2 -= dx/2 ;
			y2 -= dy/2 ;
			break ;
		case 2:
			// just 90� rotation
			x1 = ( sx - dy)/2 ;
			y1 = ( dx + sy)/2 ;
			x2 = ( sx + dy)/2 ;
			y2 = (-dx + sy)/2 ;
			break ;
		case 3:
			// 90� rotation and doubling the length
			x1 = ( sx/2 - dy) ;
			y1 = ( dx + sy/2) ;
			x2 = ( sx/2 + dy) ;
			y2 = (-dx + sy/2) ;
		}

		if (x1>=l && y1>=t && x1<r && y1<b &&
			x2>=l && y2>=t && x2<r && y2<b)
		{
			lines.push_back(pair<Point,Point>(Point(x1-l,y1-t),Point(x2-l,y2-t))) ;
		}
	}

	fclose(fp) ;
}

void FindDefects( Mat &inputImage, Mat &resImage, CParams &params, void *pData )
{
	// Color definitions
	Scalar blue(255,0,0), darkBlue(192,0,0),
		green(0,255,0), darkGreen(0,192,0),
		red(0,0,255), darkRed(0,0,192),
		magenta(255,0,255), darkMagenta(192,0,192),
		cyan(255,255,0), darkCyan(192,192,0),
		yellow(0,255,255), darkYellow(0,192,192) ;

	// Get parameters values
	bool saveIntermediateFiles = string("true") == params.GetStringValue( "General", "Save Intermediate Files" ) ;
	int cropSize = params.GetIntValue( "General", "Crop Size") ;
	int filterSize = params.GetIntValue( "Peak Detection", "Filter Size" ) ;
	double maxDist = params.GetDoubleValue( "Peak Detection", "Max Distance" ) ;
	double minCurvature = params.GetDoubleValue( "Peak Detection", "Min Curvature" ) ;
	double thresholdRatio = params.GetDoubleValue( "Peak Detection", "Threshold Ratio" ) ;
	int wideWidthThr = params.GetIntValue( "Peak Detection", "Wide Width Threshold" ) ;
	int desiredWidePeaksCount = params.GetIntValue( "Peak Detection", "Desired Wide Peaks Count" ) ;

	// Get the lines and badLinesIndex vectors
	vector<pair<Point,Point>> *pLines = (vector<pair<Point,Point>>*)(((void**)pData)[0]) ;
	vector<Point> *pDefects = (vector<Point>*)(((void**)pData)[1]) ;

	// Indicate and save the position of input lines
	if (saveIntermediateFiles){
		Mat linesImage ;
		if (1==inputImage.channels()) cvtColor( inputImage, linesImage, CV_BGR2GRAY ) ;
		else linesImage = inputImage.clone() ;
		for ( size_t i=0 ; i<pLines->size() ; i++ ){
			pair<Point,Point> &l = (*pLines)[i] ;
			line( linesImage, l.first, l.second, red ) ;
		}
		imwrite( "inputLines.png", linesImage ) ;
	}

	bool rc = InitDetector( saveIntermediateFiles, filterSize, maxDist, minCurvature,
							thresholdRatio, wideWidthThr, desiredWidePeaksCount ) ;
	int *linesBuf=NULL ;
	int maxDefectCount = int(pDefects->size()) ;
	int *badLineIndexBuf=NULL ;
	int defectCount=maxDefectCount ;
	if (rc){
		linesBuf = new int[4*pLines->size()] ;
		badLineIndexBuf = new int[maxDefectCount] ;
		if (NULL==linesBuf || NULL==badLineIndexBuf) rc=false ;
		else{
			for ( int i=0, j=0 ; i<pLines->size() ; i++ ){
				pair<Point,Point> &line = (*pLines)[i] ;
				linesBuf[j++] = line.first.x ;
				linesBuf[j++] = line.first.y ;
				linesBuf[j++] = line.second.x ;
				linesBuf[j++] = line.second.y ;
			}

			rc = Detect( (char*)(inputImage.data), inputImage.rows, inputImage.cols, inputImage.channels(),
						 linesBuf, int(pLines->size()), badLineIndexBuf, defectCount ) ;
		}
		if (rc){
			if (defectCount > 0){
				pDefects->resize(defectCount) ;
				for ( int i=0 ; i<defectCount ; i++ ){
					int j = badLineIndexBuf[i] ;
					pair<Point,Point> &line = (*pLines)[j] ;
					(*pDefects)[i] = Point( (line.first.x+line.second.x)/2, (line.first.y+line.second.y)/2 ) ;
				}
			}
			else pDefects->clear() ;
		}
		if (NULL != linesBuf) delete [] linesBuf ;
		if (NULL != badLineIndexBuf) delete [] badLineIndexBuf ;

		// Create result image
		inputImage.copyTo(resImage) ;

		// defect points indication
		int crossSize=5 ;
		for ( size_t i=0 ; i<pDefects->size() ; i++ ){
			Point &p = (*pDefects)[i] ;
			line( resImage, Point(p.x-crossSize,p.y), Point(p.x+crossSize,p.y), red ) ;
			line( resImage, Point(p.x,p.y-crossSize), Point(p.x,p.y+crossSize), red ) ;
		}

		if (saveIntermediateFiles){
			imwrite( "DefectImage.png", resImage ) ;
		}
	}
}

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CMainDlg dialog




CMainDlg::CMainDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CMainDlg::IDD, pParent)
	,m_setupFileName(".\\Setup.dat")
	,m_imageFileName(_T(""))
	,m_listFileName(_T(""))
	,m_zoomWinSize(40)
	,m_allListChanged(false)
	,m_croppedDone(false)
	,m_processDone(false)
	,m_roi(Rect(0,0,0,0))
	,m_pSetupDialog(NULL)

{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMainDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_FRAME_VIEW, m_frameView) ;
	DDX_Control(pDX, IDC_ZOOM, m_zoomWindow);
	DDX_Control(pDX, IDC_PROFILE, m_profileWindow) ;
}

BEGIN_MESSAGE_MAP(CMainDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_LOAD_IMAGE, &CMainDlg::OnBnClickedLoadImage)
	ON_BN_CLICKED(IDC_PREV_IMAGE, &CMainDlg::OnBnClickedPrevImage)
	ON_BN_CLICKED(IDC_NEXT_IMAGE, &CMainDlg::OnBnClickedNextImage)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_FIND_DEFECT, &CMainDlg::OnBnClickedFindDefect)
	ON_BN_CLICKED(IDC_SETUP, &CMainDlg::OnBnClickedSetup)
	ON_BN_CLICKED(IDC_RELOAD_IMAGE, &CMainDlg::OnBnClickedReloadImage)
	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CMainDlg message handlers

BOOL CMainDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	m_params.Load(m_setupFileName) ;

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CMainDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CMainDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CMainDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CMainDlg::OnBnClickedLoadImage()
{
	if ( SelectFile( TRUE,
					 _T("Select Image"),
					 m_imageFileName, 
					 _T("All Files (*.*)|*.*|TIFF Files (*.tiff)|*.tiff|BMP Files (*.bmp)|*.bmp|PNG Files (*.png)|*.png|JPEG Files (*.jpg)|*.jpg||"),
					 _T("General"),
					 _T("last input file"),
					 IDC_IMAGE_FILE ) )
	{
		// Set the directory name
		GetDirFiles( m_imageFileName, m_dirName, m_dirIndex ) ;

		// Load and show the image
		bool ctrlPressed = 0 != (GetAsyncKeyState(VK_CONTROL) & 0x8000) ;
		LoadAndShowImage( true ) ;
		m_croppedDone = false ;
		m_processDone = false ;
	}
}


void CMainDlg::OnBnClickedReloadImage()
{
	LoadAndShowImage( true ) ;
	m_croppedDone = false ;
	m_processDone = false ;
}


void CMainDlg::LoadAndShowImage( bool show )
{
	CWaitCursor waitCursor ;
	UpdateData(TRUE) ;
	GetDlgItem( IDC_STATUS )->SetWindowText( CString("") ) ;

	// Load the image file
	char fname[256] ;
	size_t c ;
	wcstombs_s( &c, fname, sizeof(fname), m_imageFileName.GetBuffer(), m_imageFileName.GetLength() ) ;
	String fnameStr(fname) ;
	Mat tmpImage = imread( fnameStr ) ;

	if ( !tmpImage.empty()){
		m_inputImage = tmpImage.clone() ;
	}

	bool rc = !m_inputImage.empty() ;
	if (rc){
		if (show){
			ShowImage( m_inputImage ) ;
			AdjustDialogSize() ;
		}
		GetDlgItem( IDC_STATUS )->SetWindowText( CString("Image loaded") ) ;
	}
	else{
		GetDlgItem( IDC_STATUS )->SetWindowText( CString("Couldn't load image") ) ;
	}
}



void CMainDlg::OnBnClickedPrevImage()
{
	if (m_imageFileNames.GetCount() > 1){
		m_dirIndex-- ;
		if (m_dirIndex < 0)
			m_dirIndex = __max(0,int(m_imageFileNames.GetSize())-1) ;
		m_imageFileName = m_imageFileNames[m_dirIndex] ;
		int oldWidth=0, oldHeight=0 ;
		if (!m_inputImage.empty()){
			oldWidth = m_inputImage.cols ;
			oldHeight = m_inputImage.rows ;
		}
		//LoadAndShowImageFile() ;
		LoadAndShowImage(true) ;
		if ( !m_inputImage.empty() &&
			 (m_inputImage.cols!=oldWidth || m_inputImage.rows!=oldHeight) )
		{
			AdjustDialogSize() ;
		}

		// Show the image file name in the dialog
		CWnd *pWnd = GetDlgItem( IDC_IMAGE_FILE ) ;
		if (NULL != pWnd) pWnd->SetWindowText( m_imageFileName ) ;
	}
}


void CMainDlg::OnBnClickedNextImage()
{
	if (m_imageFileNames.GetCount() > 1){
		m_dirIndex++ ;
		if (m_dirIndex >= m_imageFileNames.GetSize())
			m_dirIndex = 0 ;
		m_imageFileName = m_imageFileNames[m_dirIndex] ;
		int oldWidth=0, oldHeight=0 ;
		if (!m_inputImage.empty()){
			oldWidth = m_inputImage.cols ;
			oldHeight = m_inputImage.rows ;
		}
		LoadAndShowImage(true) ;
		if ( !m_inputImage.empty() &&
			(m_inputImage.cols!=oldWidth || m_inputImage.rows!=oldHeight) )
		{
			AdjustDialogSize() ;
		}

		// Show the image file name in the dialog
		CWnd *pWnd = GetDlgItem( IDC_IMAGE_FILE ) ;
		if (NULL != pWnd) pWnd->SetWindowText( m_imageFileName ) ;
	}
}

bool CMainDlg::SelectFile( BOOL openFlag,
						   CString title,
						   CString &fileName,
						   CString filter,
						   CString section,
						   CString key,
						   int dlgID )
{
	CFileDialog fileOpenDlg( openFlag,
		NULL,
		//NULL,
		fileName,
		OFN_OVERWRITEPROMPT | OFN_NOCHANGEDIR | OFN_EXPLORER ,
		filter,
		NULL ) ;
	CMainApp* pApp = (CMainApp*)AfxGetApp() ;
	CString tmpPath = pApp->GetProfileString( section, key, L".\\" ) ;
	fileOpenDlg.m_ofn.lpstrInitialDir = tmpPath ;
	fileOpenDlg.m_ofn.lpstrTitle = title ;
	if (IDOK == fileOpenDlg.DoModal()){
		fileName = fileOpenDlg.GetPathName() ;

		// Show the image file name in the dialog
		CWnd *pWnd = GetDlgItem( dlgID ) ;
		if (NULL != pWnd) pWnd->SetWindowText( fileName ) ;

		// Save the last file name for future call of CFileDialog activations
		pApp->WriteProfileString( section, key, fileName ) ;
		return true ;
	}
	return false ;
}


void CMainDlg::GetDirFiles( CString &fileName, CString &dirName, int &dirIndex )
{
	WIN32_FIND_DATA findFileData;
	HANDLE hFind;

	dirIndex = -1 ;
	m_imageFileNames.RemoveAll() ;
	int pos = fileName.ReverseFind('\\') ;
	if (-1 == pos) return ;

	dirName = fileName.Left(pos) ;
	wchar_t searchName[256] ;
	wchar_t *extensions[]={_T("png"),_T("jpg"),_T("bmp"),_T("tiff")} ;
	for ( int i=0 ; i<sizeof(extensions)/sizeof(*extensions) ; i++ ){
		wsprintf( searchName, _T("%s\\*.%s"), dirName.GetBuffer(),extensions[i] ) ;
		hFind = FindFirstFile( searchName, &findFileData);
		if (hFind != INVALID_HANDLE_VALUE){
			do{
				m_imageFileNames.Add( dirName + _T("\\") + CString(findFileData.cFileName) ) ;
			} while( FindNextFile( hFind, &findFileData ) ) ;
			FindClose(hFind) ;
		}
	}

	// Set the index of the given file name
	for ( int i=0 ; i<m_imageFileNames.GetCount() ; i++ ){
		if (m_imageFileNames[i] == fileName){
			dirIndex = i ;
			break ;
		}
	}
}

void CMainDlg::AdjustDialogSize()
{
	double aspectRatio=0.0 ;
	int margin=10 ; // from screen edges
	int i ;

	int imageWidth, imageHeight ;
	if (!m_inputImage.empty())
		imageWidth=m_inputImage.cols, imageHeight=m_inputImage.rows ;
	else return ;
	aspectRatio = double(imageHeight) / imageWidth ;

	// Get the available work area
	RECT workAreaRect ;
	SystemParametersInfo( SPI_GETWORKAREA, NULL, &workAreaRect, 0 ) ;

	// Get the current view rectangles
	CRect viewRect, viewClient ;
	m_frameView.GetWindowRect( &viewRect ) ;
	ScreenToClient( &viewRect ) ;

	// Calculate maximum view width and height
	CRect dlgRect, dlgClientRect ;
	GetWindowRect( &dlgRect ) ;
	GetClientRect( &dlgClientRect ) ;
	int dw = dlgRect.Width() - dlgClientRect.Width() ; // application window vertical border width X 2
	int dh = dlgRect.Height() - dlgClientRect.Height() ; // application window horizontal border width X 2
	int dx = dlgClientRect.Width() - viewRect.Width() ; // the sum of horizontal margins of the frame view
	int dy = dlgClientRect.Height() - viewRect.Height() ; // the sum of vertical margins of the frame view
	int viewWidth, viewHeight ;
	viewWidth = (workAreaRect.right - 2*margin - dw - dx) ;
	viewHeight = (workAreaRect.bottom - 2*margin - dh - dy) ;

	// Select with/height that would fit into the margins
	if (int(viewWidth*aspectRatio+0.5) < viewHeight)
		viewHeight = int(viewWidth*aspectRatio+0.5) ;
	else
		viewWidth = int(viewHeight/aspectRatio+0.5) ;

	// Limit image magnification
	m_frameView.GetClientRect( &viewClient ) ;
	int viewFrame = viewRect.Width() - viewClient.Width() ;

	// Check if adjustment is needed
	if ( viewRect.Width()==viewWidth && viewRect.Height()==viewHeight ) return ;

	// Change dialog size
	int newDlgWidth, newDlgHeight ;
	newDlgWidth = viewWidth + dw + dx ;
	newDlgHeight = viewHeight + dh + dy ;
	SetWindowPos( NULL, (workAreaRect.right-newDlgWidth)/2, (workAreaRect.bottom-newDlgHeight)/2,
		newDlgWidth, newDlgHeight, SWP_NOZORDER ) ;
	int dx2 = newDlgWidth - dlgRect.Width() ;
	int dy2 = newDlgHeight - dlgRect.Height() ;

	// Adjust components that need to be moved horizontally
	CRect buttonRect ;
	CWnd *pButton ;
	int hCompIDs[] = {IDCANCEL,IDC_ZOOM,IDC_PIX,IDC_ROI,IDC_PROFILE} ;
	for ( i=0 ; i<sizeof(hCompIDs)/sizeof(int) ; i++ ){
		pButton = GetDlgItem(hCompIDs[i]) ;
		pButton->GetWindowRect( &buttonRect ) ;
		ScreenToClient( &buttonRect ) ;
		pButton->SetWindowPos( NULL, buttonRect.left+dx2, buttonRect.top, 0, 0, SWP_NOZORDER|SWP_NOSIZE ) ;
	}

	// Adjust the main frame view positions and size
	m_frameView.SetWindowPos( NULL, viewRect.left, viewRect.top,
							  viewWidth, viewHeight, SWP_NOZORDER ) ;
}


void CMainDlg::CollectImageRect( int xPix, int yPix )
{
	Rect rect ;
	rect.width = m_zoomWinSize ;
	rect.height = m_zoomWinSize ;
	rect.x = xPix - m_zoomWinSize/2 ;
	rect.y = yPix - m_zoomWinSize/2 ;
	if ( m_inputImage.empty() ||
		rect.x<0 || rect.y<0 ||
		rect.x+rect.width>=m_inputImage.cols ||
		rect.y+rect.height>=m_inputImage.rows )
	{
		/*
		MessageBeep( MB_ICONWARNING ) ;
		MessageBox( L"Out of image limits", L"Warning!!!", MB_ICONWARNING ) ;
		*/
		return ;
	}

	Mat zoomImage ;
	m_inputImage(rect).copyTo(zoomImage) ;
	int depth=0 ;
	switch(m_inputImage.type()){
		case CV_8UC1:
		case CV_8SC1:
			depth=8; break ;
		case CV_8UC3:
		case CV_8SC3:
			depth=24; break ;
		default: return;
	}
	m_zoomWindow.SetImage( (char*)(zoomImage.data),
						   zoomImage.cols, zoomImage.rows, depth ) ;
}



void CMainDlg::OnBnClickedFindDefect()
{
	//vector<pair<Point,Point>> lines ;
	char fname[256] ;
	size_t c ;
	wcstombs_s( &c, fname, sizeof(fname), m_imageFileName.GetBuffer(), m_imageFileName.GetLength() ) ;
	string linesFileName(fname) ;
	char* extensions[] = { ".tif", ".bmp", ".png", ".jpg" } ;
	size_t nn = sizeof(extensions)/sizeof(char*) ;
	size_t i ;
	size_t pos = string::npos ;
	for ( i=0 ; i<nn ; i++ ){
		pos = linesFileName.find(extensions[i]) ;
		if (string::npos!=pos) break ;
	}
	if ( i<nn && string::npos!=pos ){
		string tmpStr = string(extensions[i]) + "__CRD.txt" ;
		linesFileName.replace( pos, string(extensions[i]).size(), tmpStr ) ;
		ReadLines( linesFileName, m_roi, m_lines ) ;

		m_defects.resize(400,Point(-1,-1)) ;
		void *pData[2] = {(void*)&m_lines,(void*)&m_defects} ;
		ProcessAndShowImage( FindDefects, (void*)(pData) ) ;
		m_processDone = true ;
	}
	else
		GetDlgItem( IDC_STATUS )->SetWindowText( CString("Can't read lines data file!") ) ;
}


void CMainDlg::ShowImage( Mat &image )
{
	static int depthTable[] = {8,8,16,15,32,32,64} ;
	if ( !image.empty() ){
		int depth = depthTable[image.depth()]*image.channels() ;
		// Change line-step in case it is not an integer multiple of 4
		int dw = image.cols%4 ;
		if (dw>0){
			Mat newImage( image.rows, image.cols+(4-dw), (image.channels()==1)?CV_8UC1:CV_8UC3 ) ;
			for ( int i=0 ; i<image.rows ; i++ ){
				 memcpy( newImage.row(i).data, image.row(i).data, image.step ) ;
			}
			newImage.copyTo(image) ;
		}
		m_frameView.SetImage( (char*)(image.data), image.cols, image.rows, depth ) ;
	}
}		


void CMainDlg::ProcessAndShowImage( void pFunc(Mat &inImage,Mat &outImage,CParams &params,void *pResData), void *pResData )
{
	CWaitCursor waitCursor ;
	UpdateData(TRUE) ;
	GetDlgItem( IDC_STATUS )->SetWindowText( CString("") ) ;
	GetDlgItem( IDC_PIX )->SetWindowText( CString("") ) ;

	// Check input image
	if ( m_inputImage.empty() ){
		GetDlgItem( IDC_STATUS )->SetWindowText( CString("No image to process!") ) ;
		return ;
	}

	CParams params ;
	params.Load(m_setupFileName) ;
	(*pFunc)( m_inputImage, m_resultImage, params, pResData ) ;

	ShowImage( m_resultImage ) ;
	AdjustDialogSize() ;
}

void CMainDlg::OnBnClickedSetup()
{
	UpdateData(FALSE) ;


	if (NULL!=m_pSetupDialog){
		m_pSetupDialog->Close() ;
		delete(m_pSetupDialog) ;
		m_pSetupDialog = NULL ;
	}
	else{
		m_pSetupDialog = new CSetupDialog( this ) ;
		if (NULL!=m_pSetupDialog){
			m_params.Load(m_setupFileName) ;
			m_pSetupDialog->m_params = m_params ;
			m_pSetupDialog->OpenDialog( false ) ;
		}
	}
	UpdateData(FALSE) ;
}

BOOL CMainDlg::OnCommand(WPARAM wParam, LPARAM lParam)
{
	int &x=m_frameView.m_xPix, &y=m_frameView.m_yPix ;

	switch( wParam ){
			case CSetupDialog::CLOSE_SETUP_WND:
		if (NULL!=m_pSetupDialog){
			m_pSetupDialog->Close() ;
			delete(m_pSetupDialog) ;
			m_pSetupDialog = NULL ;
		}
		lParam = 0 ;
		break ;
	case CSetupDialog::ACCEPT_PARAMS:
		if (NULL!=m_pSetupDialog){
			m_pSetupDialog->AcceptParams() ;
			m_pSetupDialog->m_params.Save(m_setupFileName) ;
			m_params.Load(m_setupFileName) ;
		}
		lParam = 0 ;
		break ;
	case CFrameView::PIX_DATA:
		{
			// Show pixel data
			wchar_t pixMsg[256]=_T("") ;
			if (!m_inputImage.empty()){
				if (3==m_inputImage.channels()){
					uchar *pix = (uchar*)(m_inputImage.data + y*m_inputImage.step + 3*x) ;
					if (m_croppedDone){
						swprintf( pixMsg, sizeof(pixMsg),
								  _T("Cursor data:\nlocal.pos=(%4i,%4i)\nglobal.pos=(%4i,%4i)\npix=(%3i,%3i,%3i)"),
								  x, y, x+m_roi.x, y+m_roi.y, pix[0], pix[1], pix[2] ) ;
					}
					else{
						swprintf( pixMsg, sizeof(pixMsg),
								  _T("Cursor data:\nglobal.pos=(%4i,%4i)\npix=(%3i,%3i,%3i)"),
								  x, y, pix[0], pix[1], pix[2] ) ;
					}
				}
				else{
					uchar *pix = (uchar*)(m_inputImage.data + y*m_inputImage.step + x) ;
					if (m_croppedDone){
						swprintf( pixMsg, sizeof(pixMsg), _T("Cursor data:\npos=(%i,%i)\npix=%i"), x, y, pix[0] ) ;
						swprintf( pixMsg, sizeof(pixMsg),
								  _T("Cursor data:\nlocal.pos=(%4i,%4i)\nglobal.pos=(%4i,%4i)\npix=%3i"),
								  x, y, x+m_roi.x, y+m_roi.y, pix[0] ) ;
					}
					else{
						swprintf( pixMsg, sizeof(pixMsg),
								  _T("Cursor data:\nglobal.pos=(%4i,%4i)\npix=%3i"),
								  x, y, pix[0] ) ;
					}
				}
			}
			GetDlgItem(IDC_PIX)->SetWindowTextW( pixMsg ) ;

			// Update zoom window
			CollectImageRect(x, y) ;

		}
		break ;
	case CFrameView::SELECT_POS:
		{
			if (!m_croppedDone){

				// Crop the image
				int cropSize = m_params.GetIntValue( "General", "Crop Size") ;
				m_roi = Rect(max(0,x-cropSize/2),max(0,y-cropSize/2),cropSize,cropSize) ;
				m_roi.x=min(m_inputImage.cols-cropSize,m_roi.x) ;
				m_roi.y=min(m_inputImage.rows-cropSize,m_roi.y) ;
				Mat cropedImage = m_inputImage(m_roi) ;
				cropedImage.copyTo(m_inputImage) ; // deliberately change input image
				ShowImage( m_inputImage ) ;

				CString roiText ;
				roiText.Format(_T("ROI:\nPos=(%i,%i)\nSize=(%i,%i)"),x,y,cropSize,cropSize) ;
				GetDlgItem(IDC_ROI)->SetWindowTextW(roiText) ;

				AdjustDialogSize() ;

				m_croppedDone = true ;
			}
			else if (m_processDone){
				// Find the closest profile line
				double minDist=DBL_MAX, xcMin, ycMin ;
				int iMin=-1 ;
				for ( size_t i=0 ; i<m_lines.size() ; i++ ){
					pair<Point,Point> &line = m_lines[i] ;
					Point &p1=line.first, &p2=line.second ;
					// Find the distance of (x,y) from the center of the current line
					double xc=0.5*(p1.x+p2.x), yc=0.5*(p1.y+p2.y) ;
					double dx1p=p1.x-x, dy1p=p1.y-y;
					double dist = _hypot(x-xc,y-yc) ;
					if (dist < minDist){
						minDist = dist ;
						xcMin=xc, ycMin=yc ;
						iMin=int(i) ;
					}
				}
				if (iMin>-1){
					int filterSize = m_params.GetIntValue( "Peak Detection", "Filter Size" ) ;
					double maxDist = m_params.GetDoubleValue( "Peak Detection", "Max Distance" ) ;
					double MinCurvature = m_params.GetDoubleValue( "Peak Detection", "Min Curvature" ) ;
					double thresholdRatio = m_params.GetDoubleValue( "Peak Detection", "Threshold Ratio" ) ;
					int wideWidthThr = m_params.GetIntValue( "Peak Detection", "Wide Width Threshold" ) ;
					int desiredWidePeaksCount = m_params.GetIntValue( "Peak Detection", "Desired Wide Peaks Count" ) ;

					Mat greyImage ;
					if (3==m_inputImage.channels()) cvtColor( m_inputImage, greyImage, CV_BGR2GRAY ) ;
					else m_inputImage.copyTo(greyImage) ;
					
					pair<Point,Point> &line = m_lines[iMin] ;
					int pLine[4] = {line.first.x,line.first.y,line.second.x,line.second.y} ;
					const int maxLineValuesCount=256 ;
					unsigned char pLineValues[maxLineValuesCount] ;
					int lineValuesCount = maxLineValuesCount ;
					CollectSingleLineValues( (char*)(greyImage.data), greyImage.cols, greyImage.rows, greyImage.channels(),
											 pLine, pLineValues, lineValuesCount ) ;
					vector<uchar> lineValues(lineValuesCount) ;
					for ( int i=0 ; i<lineValuesCount ; i++ ) lineValues[i] = pLineValues[i] ;
					int pPeakIdxes[2] ;
					int peakIdxesCount=0 ;
					int minVal, maxVal ;
					double thrVal ;
					bool wideLine ;
					FindPeaks( pLineValues, lineValuesCount, minVal, maxVal, thrVal, wideLine, pPeakIdxes, peakIdxesCount ) ;

					// Find average and median values of pLineValues
					vector<uchar> sortedVals = lineValues ;
					sort( sortedVals.begin(), sortedVals.end() ) ;
					double medVal = sortedVals[lineValuesCount/2] ;
					double sum=0 ;
					for ( int i=0 ; i<lineValuesCount ; i++ ) sum+=pLineValues[i] ;
					double avrgVal = sum/lineValuesCount ;

					vector<int> peaksIdxes(peakIdxesCount) ;
					for ( int i=0 ; i<peakIdxesCount ; i++ ) peaksIdxes[i] = pPeakIdxes[i] ;
					Point selectedPointLocal(int(xcMin+.5),int(ycMin+.5)) ;
					Point selectedPointGlobal = selectedPointLocal + Point(m_roi.x,m_roi.y) ;
					m_profileWindow.SetProfile( lineValues, selectedPointLocal.x, selectedPointLocal.y, selectedPointGlobal.x, selectedPointGlobal.y ) ;
					m_profileWindow.SetMinMaxThrAvrgMed( minVal, maxVal, thrVal, avrgVal, medVal ) ;
					m_profileWindow.SetPeaksIndexes( peaksIdxes ) ;
					m_profileWindow.Invalidate(0) ;

					// Show the cropped image with all the bad points in red and the closest point in green
					// Color definitions
					m_resultImage = m_inputImage.clone() ;
					Scalar blue(255,0,0), green(0,255,0), red(0,0,255) ;
					int crossSize=5 ;
					for ( size_t i=0 ; i<m_defects.size() ; i++ ){
						Point &pos = m_defects[i] ;
						if ( pos != Point(-1,-1) ){
							Point p1(pos.x-crossSize,pos.y), p2(pos.x+crossSize,pos.y) ;
							Point p3(pos.x,pos.y-crossSize), p4(pos.x,pos.y+crossSize) ;
							cv::line( m_resultImage, p1, p2, red ) ;
							cv::line( m_resultImage, p3, p4, red ) ;
						}
					}
					Point p1(selectedPointLocal.x-2*crossSize,selectedPointLocal.y), p2(selectedPointLocal.x+2*crossSize,selectedPointLocal.y) ;
					Point p3(selectedPointLocal.x,selectedPointLocal.y-2*crossSize), p4(selectedPointLocal.x,selectedPointLocal.y+2*crossSize) ;
					cv::line( m_resultImage, p1, p2, green ) ;
					cv::line( m_resultImage, p3, p4, green ) ;

					ShowImage( m_resultImage ) ;
					AdjustDialogSize() ;

				}
			}
		}
		break ;

	case CGraphicView::SELECT_POS:
		if (m_croppedDone && m_processDone && NULL!=m_profileWindow ){
			int profileIdx = m_profileWindow.m_point.x ;
			int filterSize = m_params.GetIntValue( "Peak Detection", "Filter Size" ) ;
			double a, b, c ;
			int profileSize = int(m_profileWindow.m_profile.size()) ;
			unsigned char *pLineValues = new unsigned char[profileSize] ;
			if (NULL!=pLineValues){
				for ( int i=0 ; i<profileSize ; i++ ) pLineValues[i]=m_profileWindow.m_profile[i] ;
				GetParabolicParams( pLineValues, profileSize, filterSize, profileIdx, a, b, c ) ;
				wchar_t pixMsg[256] ;
				swprintf( pixMsg, sizeof(pixMsg),
						  _T("Parabola Params:\na=%.2f\nb/2a=%.2f\nc-b^2/4a=%.2f"),
						  a, b/(2*a), c-b*b/(4*a) );
				GetDlgItem(IDC_PIX)->SetWindowTextW( pixMsg ) ;
				delete [] pLineValues ;
			}
		}
		break ;

	}

	lParam = 0 ;
	return CDialogEx::OnCommand(wParam, lParam);
}


void CMainDlg::OnClose()
{
	if (NULL!=m_pSetupDialog){
		m_pSetupDialog->Close() ;
		delete(m_pSetupDialog) ;
		m_pSetupDialog = NULL ;
	}

	CDialogEx::OnClose();
}
