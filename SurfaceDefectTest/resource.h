//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by SurfaceDefectTest.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_SURFACE_DEFECT_DIALOG       102
#define IDR_MAINFRAME                   128
#define IDC_LOAD_IMAGE                  1000
#define IDC_PREV_IMAGE                  1001
#define IDC_NEXT_IMAGE                  1002
#define IDC_ZOOM                        1003
#define IDC_IMAGE_FILE                  1004
#define IDC_FRAME_VIEW                  1005
#define IDC_PIX                         1006
#define IDC_IMAGE_FILE2                 1007
#define IDC_STATUS                      1007
#define IDC_ROI                         1008
#define IDC_FIND_DEFECT                 1019
#define IDC_SETUP                       1020
#define IDC_RELOAD_IMAGE                1021
#define IDC_PROFILE                     1022

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1023
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
