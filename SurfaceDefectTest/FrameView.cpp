// FrameView.cpp : implementation file
//

#include "stdafx.h"
#include <math.h>
#include "FrameView.h"
#include <AtlImage.h>


#define emptyRect CRect(0,0,0,0)
#define isLegalRect(r) ((r).left>0 && (r).left<(r).right && (r).top>0 && (r).top<(r).bottom)
#define emptyPoint CPoint(0,0)
#define isLegalPoint(p) ((p).x>0 && (p).y>0)
#define unchangedRect CRect(-1,-1,-1,-1)
#define unchangedPoint CPoint(-1,-1)
#define IgnoreRectInput(r) ((-1==(r).left)&&(-1==(r).right)&&(-1==(r).top)&&(-1==(r).bottom))
#define IgnorePointInput(p) ((-1==(p).x)&&(-1==(p).y))


IMPLEMENT_DYNAMIC(CFrameView, CWnd)

CFrameView::CFrameView()
: m_xPix(0)
, m_yPix(0)
, m_firstPos(CPoint(0, 0))
, m_lastPos(CPoint(0, 0))
, m_xScale(1.0)
, m_yScale(1.0)
{
	m_imageBuff = new CImage;
}

CFrameView::~CFrameView()
{
	//image.Destroy() ;
	if (m_imageBuff){
		((CImage*)m_imageBuff)->Destroy();
		delete m_imageBuff;
	}
}


BEGIN_MESSAGE_MAP(CFrameView, CWnd)
	ON_WM_PAINT()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_SIZE()
	ON_WM_LBUTTONDBLCLK()
END_MESSAGE_MAP()



// CFrameView message handlers

void CFrameView::SetImage(char *data, int width, int height, int depth)
{
	if (NULL != data){ // Prepare CImage
		CImage *pImage = ((CImage*)m_imageBuff);
		if (!pImage->IsNull() &&
			(pImage->GetWidth() != width || pImage->GetHeight() != height || pImage->GetBPP() != depth))
		{
			pImage->Destroy();
		}

		if (pImage->IsNull()){
			pImage->Create(width, -height, depth, 0);
			if (8 == depth){
				// Set a gray color palette
				RGBQUAD *colorTable = new RGBQUAD[256];
				if (NULL != colorTable){
					for (int c = 0; c<256; c++)
						((int*)(colorTable))[c] = c * 0x00010101;
					pImage->SetColorTable(0, 256, colorTable);
					delete[] colorTable;
				}
			}
		}

		if (!pImage->IsNull()){
			SetScales();
			BYTE* dataPtr = (BYTE*)pImage->GetBits();
			memcpy(dataPtr, data, width*height*(depth >> 3));
			Invalidate();
		}
	}
}

void CFrameView::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	if (m_imageBuff){
		CImage *pImage = ((CImage*)m_imageBuff);
		if (!pImage->IsNull()){
			RECT windowRect, clientRect;
			GetWindowRect(&windowRect);
			ScreenToClient(&windowRect);
			GetClientRect(&clientRect);
			CDC *pDC = GetDC();
			if (NULL != pDC && NULL != pDC->m_hDC){
				pDC->SetStretchBltMode(COLORONCOLOR);
				pImage->StretchBlt(pDC->m_hDC, clientRect);
				ReleaseDC(pDC);
			}

			/*
			COLORREF colors[] = {0x0000ff00,  // green
			0x00ff0000,  // blue
			0x000000ff,  // red
			0x00ffff00,  // cyan
			0x00ff00ff,  // magenta
			0x0000ffff,  // yellow
			0x00cffffcf} ;// gray
			int colorCount = sizeof(colors)/sizeof(colors[0]) ;
			*/
			GetClientRect(&clientRect);
			CRgn rgn;
			rgn.CreateRectRgn(clientRect.left, clientRect.top, clientRect.right, clientRect.bottom);
			dc.SelectClipRgn(&rgn);
		}
	}
}

bool CFrameView::TranslateToPixels(CPoint &point)
{
	RECT clientRect;
	GetClientRect(&clientRect);
	if (point.x >= 0 && point.y >= 0 &&
		point.x<clientRect.right &&
		point.y<clientRect.bottom)
	{
		m_xPix = int(0.5 + point.x*m_xScale);
		m_yPix = int(0.5 + point.y*m_yScale);
		return true;
	}
	return false;
}

void CFrameView::OnLButtonDown(UINT nFlags, CPoint point)
{
	if (m_imageBuff){
		if (isLegalPoint(point)){
			TranslateToPixels(point);
			m_firstPos = point;
			if (MK_CONTROL&nFlags){ // LEFT_DOWN_CONTROL
				::PostMessage(GetParent()->m_hWnd, WM_COMMAND, SELECT_POS, NULL);//(LPARAM)this ) ;
			}
			else if (MK_SHIFT&nFlags){ // LEFT_DOWN_SHIFT
			}
			else{
			}
		}
	}

	CWnd::OnLButtonDown(nFlags, point);
}

void CFrameView::OnLButtonUp(UINT nFlags, CPoint point)
{
	if (m_imageBuff){
		m_firstPos = m_lastPos = emptyPoint;
	}
	CWnd::OnLButtonUp(nFlags, point);
}

void CFrameView::OnMouseMove(UINT nFlags, CPoint point)
{
	if (m_imageBuff){
		if (isLegalPoint(point)){
			TranslateToPixels(point);
			if (nFlags&MK_LBUTTON){
				m_lastPos = point;
			}
			else if (nFlags == 0){
				Invalidate();
				::PostMessage(GetParent()->m_hWnd, WM_COMMAND, PIX_DATA, (LPARAM)this);
			}
		}
	}
	CWnd::OnMouseMove(nFlags, point);
}

void CFrameView::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);
	SetScales();
}

void CFrameView::SetScales()
{
	if (m_imageBuff){
		CImage *pImage = ((CImage*)m_imageBuff);
		if (!pImage->IsNull()){
			RECT clientRect;
			GetClientRect(&clientRect);
			double old_xScale = m_xScale, old_yScale = m_yScale;
			m_xScale = pImage->GetWidth() / double(clientRect.right);
			m_yScale = pImage->GetHeight() / double(clientRect.bottom);

			// update graphics
			double rx = old_xScale / m_xScale, ry = old_yScale / m_yScale;
		}
	}
}

