
// MainDlg.h : header file
//

#pragma once

#include <vector>
#include <algorithm>
#include <string>

using namespace std;

#include <opencv.hpp>
using namespace cv;

#include <params.h>
#include <SetupDialog.h>
#include "FrameView.h"
#include "GraphicView.h"
#include "afxwin.h"

// CMainDlg dialog
class CMainDlg : public CDialogEx
{
	// Construction
public:
	CMainDlg(CWnd* pParent = NULL);	// standard constructor

	// Dialog Data
	enum { IDD = IDD_SURFACE_DEFECT_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	bool SelectFile(BOOL openFlag,
		CString title,
		CString &fileName,
		CString filter,
		CString section,
		CString key,
		int dlgID);
	void GetDirFiles(CString &fileName, CString &dirName, int &dirIndex);
	void ShowImage(Mat &image);
	void LoadAndShowImage(bool show);
	void AdjustDialogSize();
	void CollectImageRect(int xPix, int yPix);

	// Implementation
protected:
	HICON m_hIcon;
	CParams m_params;
	string m_setupFileName;
	CString m_imageFileName;
	CArray<CString, CString> m_imageFileNames;
	CString m_dirName;
	int m_dirIndex;
	Mat m_inputImage;
	Mat m_resultImage;
	CFrameView m_frameView;
	CFrameView m_zoomWindow;
	CGraphicView m_profileWindow;
	int m_zoomWinSize;
	bool m_allListChanged;
	CString m_listFileName;
	int m_imageListIdx;
	bool m_croppedDone;
	bool m_processDone;
	Rect m_roi;
	vector<pair<Point, Point>> m_lines;
	vector<Point> m_defects;
	CSetupDialog *m_pSetupDialog;


	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
	afx_msg void OnBnClickedLoadImage();
	afx_msg void OnBnClickedReloadImage();
	afx_msg void OnBnClickedPrevImage();
	afx_msg void OnBnClickedNextImage();
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	afx_msg void OnBnClickedFindDefect();
	afx_msg void OnBnClickedSetup();

	void ProcessAndShowImage(void pFunc(Mat &inImage, Mat &outImage, CParams &params, void *pResData), void *pResData = NULL);
public:
	afx_msg void OnClose();
};
