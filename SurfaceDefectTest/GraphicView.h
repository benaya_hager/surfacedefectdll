#pragma once

#include <vector>
#include <algorithm>
#include <string>

using namespace std;

// CGraphicView

class CGraphicView : public CWnd
{
	DECLARE_DYNAMIC(CGraphicView)

public:
	CGraphicView();
	virtual ~CGraphicView();
	void SetScales() ;

public:
	enum { START_MOVE=WM_USER+200, SELECT_POS } ;
	CPoint m_firstPos, m_lastPos ;
	vector<unsigned char> m_profile ;
	vector<int> m_peaksIdxes ;
	int m_minVal, m_maxVal ;
	double m_thrVal, m_avrgVal, m_medVal ;
	CPoint m_point ;
	CPoint m_centerLocal, m_centerGlobal ;

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnSize(UINT nType, int cx, int cy);

public:
	//void SetProfile( vector<unsigned char> &profile, int xCenter, int yCenter ) { m_profile=profile; m_center.x=xCenter; m_center.y=yCenter; }
	void SetProfile( vector<unsigned char> &profile, int xCenterLocal, int yCenterLocal, int xCenterGlobal, int yCenterGlobal )
	{
		m_profile=profile; m_centerLocal.x=xCenterLocal; m_centerLocal.y=yCenterLocal; m_centerGlobal.x=xCenterGlobal; m_centerGlobal.y=yCenterGlobal;
	}
	void SetMinMaxThr( int minVal, int maxVal, double thrVal ) { m_minVal=minVal; m_maxVal=maxVal; m_thrVal=thrVal; }
	void SetMinMaxThrAvrgMed( int minVal, int maxVal, double thrVal, double avrgVal, double medVal  ) { m_minVal=minVal; m_maxVal=maxVal; m_thrVal=thrVal; m_avrgVal=avrgVal; m_medVal=medVal; }
	void SetPeaksIndexes( vector<int> &peaksIdxes ) { m_peaksIdxes=peaksIdxes; }
};


