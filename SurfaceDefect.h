#ifdef DETECTOR_EXPORTS
#define DETECTOR_API extern "C" __declspec(dllexport)
#else
#define DETECTOR_API __declspec(dllimport)
#endif

// The InitDetector adds a new detector if needed, and sets it parameters
extern "C"{
	DETECTOR_API bool InitDetector( bool saveIntermediate,
									int filterSize,
									double maxDist,
									double minCurvature,
									double thresholdRatio,
									int wideWidthThr,
									int desiredWidePeaksCount ) ;
}

// The Detect function runs the detection function on a given image, collects and reports the results.
extern "C" {
	DETECTOR_API bool Detect( char *imageData,
							  int width,
							  int height,
							  int channels,
							  int *pLines,
							  int lineCount,
							  int *pBadLineIndexes,
							  int &badCount ) ;
}

extern "C" {
	DETECTOR_API void FindPeaks( unsigned char *pLineValues,
								 int lineValuesSize,
								 int &minVal,
								 int &maxVal,
								 double &thrVal,
								 bool &wideLine,
								 int *pPeakIdxes,
								 int &peakIdxesCount ) ;
}

extern "C" {
	DETECTOR_API void CollectSingleLineValues( char *imageData, int width, int height, int channels,
											   int *pLine, unsigned char *pLineValues, int &lineValuesCount ) ;
}

extern "C" {
	DETECTOR_API void GetParabolicParams( unsigned char *profileData, int &profileDataSize,
										  int profileIdx, double &a, double &b, double &c ) ;
}